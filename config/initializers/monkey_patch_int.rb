class Integer
  def r_or_l?
    [0, 3].include? self
  end

  def u_or_d?
    [1, 4].include? self
  end

  def f_or_b?
    [2, 5].include? self
  end
end