Rails.application.routes.draw do
  # For details on the DSL available within this file,
  # see http://guides.rubyonrails.org/routing.html

  root 'pages#home'
  post 'api/step', to: 'api#step'
  post 'api/step1', to: 'api#step1'
  get 'api/test', to: 'api#test'
end
