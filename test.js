let itemss = [
  { quantity: 50, width: 2, height: 1, product: { area_price: 0.8 } },
  { quantity: 50, width: 1, height: 1, product: { area_price: 0.8 } },
];

function quantityDiscount(items) {
  let totalNormalPrice = 0;
  let totalQuantity = 0;
  items.forEach(item => {
    const area = item.width * item.height;
    const quantityAdjustment = Math.pow(item.quantity, -0.1);
    const itemPrice = area * item.product.area_price * item.quantity * quantityAdjustment;
    totalNormalPrice += itemPrice;
    totalQuantity += item.quantity;
  });
  totalNormalPrice = Math.round(totalNormalPrice * 100) / 100;
  let totalReducedPrice = 0;
  const quantityAdjustment = Math.pow(totalQuantity, -0.1);
  items.forEach(item => {
    const area = item.width * item.height;
    const itemPrice = area * item.product.area_price * item.quantity * quantityAdjustment;
    totalReducedPrice += itemPrice;
  });
  totalReducedPrice = Math.round(totalReducedPrice * 100) / 100;
  return totalReducedPrice - totalNormalPrice;
}

Console.log(quantityDiscount(itemss));