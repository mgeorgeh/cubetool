require "rails_helper"

RSpec.describe StateIntEncoding do
  describe "int" do
    it "" do
      sc = "L2 D2 R2 D2 B R D2 B' R U B2 U F' U L' B2 U' R' D2 R2 F2 D F2 L U".tr(" ", "") +
        "LRF2UB" + "FU'R2UR'F" + "B'RB"
      st = CubeState.from_scramble(sc)
      s4st = MethodStepExtractor.new(
        state_type: PetrusS4State,
        cube_state: st,
      ).extract
      i = StateIntEncoding.new(s4st).int
      expect(i).to be == 569960
    end
  end
end