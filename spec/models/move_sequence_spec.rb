require "rails_helper"

RSpec.describe MoveSequence do
  describe "valid?" do
    it "parses move strings correctly" do
      str = "RUR'U'R'F2"
      expect(MoveSequence.from_string(str).valid?).to eql(true)
    end
    it "fails on incorrect sequences" do
      ["R''F", "RUBHJ", "22R"].each do |s|
        expect(MoveSequence.from_string(s).valid?).to eql(false)
      end
    end
  end

  describe "calc_moves" do
    it "generates correct move sequences" do
      str = "RUR'U'R'F2L'B2D"
      result = [R, U, Ri, Ui, Ri, F2, Li, B2, D]
      expect(MoveSequence.from_string(str).moves).to eql(result)
    end
  end
end
