require "rails_helper"

RSpec.describe PossibleStatesGenerator do
  describe "states" do
    it "" do
      sts = PossibleStatesGenerator.new(Cep1State).states
      expect(sts.length).to be == 384
      sts = PossibleStatesGenerator.new(PetrusS3State).states
      expect(sts.length).to be == 64
    end
  end
end
