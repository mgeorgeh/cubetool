require "rails_helper"

RSpec.describe EdgeSet do
  scrambled_state = [
    [[4, 4, 2], [1, 0, 5], [2, 0, 1]], 
    [[1, 3, 3], [0, 1, 3], [3, 0, 4]], 
    [[5, 5, 5], [3, 2, 5], [1, 0, 4]], 
    [[0, 2, 5], [1, 3, 2], [2, 4, 4]], 
    [[0, 3, 0], [1, 4, 2], [0, 2, 3]], 
    [[1, 5, 3], [1, 5, 4], [5, 4, 2]]
  ]

  invalid_scramble = [
    [[4, 4, 2], [1, 0, 5], [2, 0, 1]], 
    [[1, 3, 3], [5, 1, 3], [3, 0, 4]], 
    [[5, 0, 5], [3, 2, 5], [1, 0, 4]], 
    [[0, 2, 5], [1, 3, 2], [2, 4, 4]], 
    [[0, 3, 0], [1, 4, 2], [0, 2, 3]], 
    [[1, 5, 3], [1, 5, 4], [5, 4, 2]]
  ]

  describe "orientation_parity?" do
    it "succeeds on the solved state" do
      s = CubeState.solved_state.edges
      expect(s.orientation_parity?).to eql(true)
    end

    it "succeeds on a valid scramble" do
      s = CubeState.from_array(scrambled_state).edges
      expect(s.orientation_parity?).to eql(true)
    end

    it "fails on an invalid scramble" do
      s = CubeState.from_array(invalid_scramble).edges
      expect(s.orientation_parity?).to eql(false)
    end
  end
end