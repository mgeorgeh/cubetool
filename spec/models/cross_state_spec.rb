require "rails_helper"

RSpec.describe CrossState do
  describe "solutions" do
    it "" do
      st = CrossState.random
      soln = st.solutions[0]
      expect(CrossState.by_applying(soln, to: st)).to be == CrossState.solved_state
    end
  end
end
