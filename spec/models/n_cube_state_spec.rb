# require "rails_helper"

# RSpec.describe NCubeState do
#   describe "applying_move" do
#     it "applying single move 4 times should do nothing" do
#       ['R', 'Ri', 'U', 'Ui', 'F', 'Fi', 'L', 'Li', 'D', 'Di', 'B', 'Bi'].each do |m|
#         start_state = NCubeState.solved_state
#         end_state = start_state.applying_move(m)
#           .applying_move(m)
#           .applying_move(m)
#           .applying_move(m)
#         expect(end_state).to be == start_state
#       end
#     end
#     it "applying double turns twice should do nothing" do
#       ['R2', 'U2', 'F2', 'L2', 'D2', 'B2'].each do |m|
#         start_state = NCubeState.solved_state
#         end_state = start_state.applying_move(m).applying_move(m)
#         expect(end_state).to be == start_state
#       end
#     end
#   end

#   describe "edge_solved" do
#     it "all edges are solved on the solved state" do
#       start = NCubeState.solved_state
#       ['RU', 'RF', 'RB', 'RD', 'UF', 'UB', 'UL', 'FD', 'FL', 'BL', 'BD', 'LD'].each do |edge|
#         expect(start.edge_solved?(edge)).to be == true
#       end
#     end
#   end

#   describe "applying_move" do
#     it "" do
#       start = NCubeState.new([
#         [[1,2,3],[4,5,6],[7,8,9]],
#         [[0,0,0],[0,0,0],[0,0,0]],
#         [[0,0,0],[0,0,0],[0,0,0]],
#         [[0,0,0],[0,0,0],[0,0,0]],
#         [[0,0,0],[0,0,0],[0,0,0]],
#         [[0,0,0],[0,0,0],[0,0,0]]
#       ])
#       after = start.applying_move('R')
#       result = [[7,4,1],[8,5,2],[9,6,3]]
#       expect(after.arr[0]).to be == result
#     end
#   end

#   describe "from_scramble" do
#     it "does the scrambling" do
#       # seq = MoveSequence.new("R'B2DL'F'L'D'LU2R'D2B'RFR'BD2R2D2RDR'FL'B2")
#       seq = MoveSequence.new("FB'RB'L2RD'R2L2B'RF2U'LURUR2DF'D2U'B2L2F2")
#       # R' U2 L' B
#       result_arr = [
#         [[0, 2, 0], [4, 0, 3], [3, 5, 2]], 
#         [[2, 3, 2], [3, 1, 5], [4, 1, 2]], 
#         [[0, 1, 5], [0, 2, 5], [0, 3, 1]], 
#         [[4, 2, 4], [4, 3, 2], [5, 2, 5]], 
#         [[1, 0, 3], [5, 4, 0], [5, 1, 1]], 
#         [[3, 0, 1], [4, 5, 4], [4, 1, 3]]
#       ]
#       expect(NCubeState.from_scramble(seq)).to be == NCubeState.new(result_arr)
#       seq = MoveSequence.new("BDR2U2F'R2F2LR'D2L'RF'U'RFDBL'U2D'FR2U'D2")
#       result_arr = [
#         [[4, 4, 1], [3, 0, 2], [1, 5, 3]], 
#         [[0, 3, 0], [0, 1, 1], [5, 5, 0]], 
#         [[4, 4, 0], [1, 2, 4], [3, 1, 5]], 
#         [[2, 2, 1], [3, 3, 2], [4, 0, 4]], 
#         [[2, 5, 5], [0, 4, 4], [3, 3, 2]], 
#         [[5, 5, 2], [1, 5, 0], [3, 2, 1]]
#       ]
#       expect(NCubeState.from_scramble(seq)).to be == NCubeState.new(result_arr)
#     end
#   end
# end
