require "rails_helper"

RSpec.describe Corner do
  describe "clockwise_twists_to_orient" do
    it "correctly determines corner orientation" do
      c = Corner.from_arrs([0,2,1],[4,3,2])
      expect(c.clockwise_twists_to_orient).to eql(2)
      c = Corner.from_arrs([0,1,5],[0,2,1])
      expect(c.clockwise_twists_to_orient).to eql(2)
      c = Corner.from_arrs([0,4,2],[2,3,1])
      expect(c.clockwise_twists_to_orient).to eql(2)
      c = Corner.from_arrs([0,5,4],[3,4,5])
      expect(c.clockwise_twists_to_orient).to eql(1)
      c = Corner.from_arrs([3,1,2],[0,1,5])
      expect(c.clockwise_twists_to_orient).to eql(0)
      c = Corner.from_arrs([3,5,1],[2,0,4])
      expect(c.clockwise_twists_to_orient).to eql(0)
      c = Corner.from_arrs([3,2,4],[1,3,5])
      expect(c.clockwise_twists_to_orient).to eql(2)
      c = Corner.from_arrs([3,4,5],[5,4,0])
      expect(c.clockwise_twists_to_orient).to eql(0)
    end
  end
end
