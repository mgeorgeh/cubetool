require "rails_helper"

RSpec.describe CubeState do

  scrambled_state = [
    [[4, 4, 2], [1, 0, 5], [2, 0, 1]], 
    [[1, 3, 3], [0, 1, 3], [3, 0, 4]], 
    [[5, 5, 5], [3, 2, 5], [1, 0, 4]], 
    [[0, 2, 5], [1, 3, 2], [2, 4, 4]], 
    [[0, 3, 0], [1, 4, 2], [0, 2, 3]], 
    [[1, 5, 3], [1, 5, 4], [5, 4, 2]]
  ]

  describe "valid?" do
    it "fails on an array with invalid entries" do
      arr = Array.new(50, 1) + [8] + Array.new(3, 2)
      expect(CubeState.from_array(arr).valid?).to eql(false)
    end
    it "fails on an array with length != 54" do
      arr = Array.new(53, 1)
      expect(CubeState.from_array(arr).valid?).to eql(false)
      arr = Array.new(55, 1)
      expect(CubeState.from_array(arr).valid?).to eql(false)
    end
    it "succeeds on the solved state" do
      expect(CubeState.solved_state.valid?).to eql(true)
    end
    it "succeeds on a valid scramble" do
      expect(CubeState.from_array(scrambled_state).valid?).to eql(true)
      expect(CubeState.from_scramble(Scramble.new).valid?).to eql(true)
    end
  end
end
