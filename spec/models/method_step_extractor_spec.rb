require 'rails_helper'

RSpec.describe MethodStepExtractor do
  describe "extract" do
    it "" do
      state1 = CrossState.solved_state
      # state2 = CrossState.from_cube_state(CubeState.solved_state)
      state2 = MethodStepExtractor.new(
        state_type: CrossState,
        cube_state: CubeState.solved_state,
      ).extract
      expect(state1).to be == state2
    end

    it "" do
      st = CubeState.from_scramble("FR2F2R'F'UF'L2B2LB2RF'RFR'D'B'D'BDB2LD'F2")
      state1 = MethodStepExtractor.new(
        state_type: CrossState,
        cube_state: st,
      ).extract
      state2 = CrossState.new({
        'DR' => Edge.from_arrs([3, 1], [4, 0]),
        'DB' => Edge.from_arrs([1, 2], [4, 5]),
        'DL' => Edge.from_arrs([4, 5], [4, 3]),
        'DF' => Edge.from_arrs([3, 4], [4, 2]),
      })
      expect(state1).to be == state2
    end

  end
end