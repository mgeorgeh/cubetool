require "rails_helper"

RSpec.describe Edge do
  describe "flips_to_orient" do
    it "correctly determines orientation of edges" do
      e = Edge.from_arrs([0,1],[2,3])
      expect(e.flips_to_orient).to eql(1)
      e = Edge.from_arrs([0,4],[2,0])
      expect(e.flips_to_orient).to eql(1)
      e = Edge.from_arrs([0,2],[5,0])
      expect(e.flips_to_orient).to eql(1)
      e = Edge.from_arrs([0,5],[5,3])
      expect(e.flips_to_orient).to eql(1)
      e = Edge.from_arrs([1,2],[5,1])
      expect(e.flips_to_orient).to eql(1)
      e = Edge.from_arrs([1,5],[0,1])
      expect(e.flips_to_orient).to eql(1)
      e = Edge.from_arrs([4,2],[4,5])
      expect(e.flips_to_orient).to eql(0)
      e = Edge.from_arrs([4,5],[2,4])
      expect(e.flips_to_orient).to eql(1)
      e = Edge.from_arrs([3,1],[1,2])
      expect(e.flips_to_orient).to eql(1)
      e = Edge.from_arrs([3,4],[0,4])
      expect(e.flips_to_orient).to eql(0)
      e = Edge.from_arrs([3,2],[3,4])
      expect(e.flips_to_orient).to eql(0)
      e = Edge.from_arrs([3,5],[3,1])
      expect(e.flips_to_orient).to eql(0)
    end
  end
end