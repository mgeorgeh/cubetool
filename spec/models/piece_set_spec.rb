require "rails_helper"

RSpec.describe PieceSet do

  scrambled_state = [
    [[4, 4, 2], [1, 0, 5], [2, 0, 1]], 
    [[1, 3, 3], [0, 1, 3], [3, 0, 4]], 
    [[5, 5, 5], [3, 2, 5], [1, 0, 4]], 
    [[0, 2, 5], [1, 3, 2], [2, 4, 4]], 
    [[0, 3, 0], [1, 4, 2], [0, 2, 3]], 
    [[1, 5, 3], [1, 5, 4], [5, 4, 2]]
  ]

  describe "swap_count" do
    it "counts 0 swaps on the solved state" do
      s = CubeState.solved_state.corners
      expect(s.swap_count).to eql(0)
      s = CubeState.solved_state.edges
      expect(s.swap_count).to eql(0)
    end
    it "counts the correct number of swaps on a scrambled state" do
      s = CubeState.from_array(scrambled_state).corners
      expect(s.swap_count).to eql(5)
      s = CubeState.from_array(scrambled_state).edges
      expect(s.swap_count).to eql(9)
    end
  end
end

#<Edge:0x007fac730858a0 @data={0=>4, 1=>0}>, A
#<Edge:0x007fac73085468 @data={0=>1, 2=>5}>, A
#<Edge:0x007fac73084fb8 @data={0=>5, 5=>4}>, A
#<Edge:0x007fac730848b0 @data={0=>0, 4=>2}>, A
#<Edge:0x007fac73084540 @data={1=>0, 2=>5}>, A
#<Edge:0x007fac73084130 @data={1=>3, 5=>5}>, A
#<Edge:0x007fac6fc07e20 @data={3=>2, 1=>3}>, B
#<Edge:0x007fac6fc07b50 @data={3=>1, 2=>3}>, B
#<Edge:0x007fac6fc07880 @data={3=>2, 5=>1}>, A
#<Edge:0x007fac6fc075b0 @data={3=>4, 4=>3}>, C
#<Edge:0x007fac6fc072e0 @data={2=>0, 4=>1}>, A
#<Edge:0x007fac6fc07010 @data={5=>4, 4=>2}>, A

# 40 -> 02 -> 15 -> 35 -> 21 -> 05 -> 54 -> 42 -> 01 -> 40

#<Set: {4, 0}>, #<Set: {0, 2}>, #<Set: {1, 5}>, #<Set: {3, 5}>, #<Set: {2, 1}>, #<Set: {0, 5}>, #<Set: {5, 4}>, #<Set: {4, 2}>, #<Set: {0, 1}
#<Set: {1, 5}>, #<Set: {3, 5}>, #<Set: {2, 1}>, #<Set: {0, 5}>, #<Set: {5, 4}>, #<Set: {4, 2}>, #<Set: {0, 1}>, #<Set: {4, 0}>, #<Set: {0, 2}
#<Set: {5, 4}>, #<Set: {4, 2}>, #<Set: {0, 1}>, #<Set: {4, 0}>, #<Set: {0, 2}>, #<Set: {1, 5}>, #<Set: {3, 5}>, #<Set: {2, 1}>, #<Set: {0, 5}
#<Set: {0, 2}>, #<Set: {1, 5}>, #<Set: {3, 5}>, #<Set: {2, 1}>, #<Set: {0, 5}>, #<Set: {5, 4}>, #<Set: {4, 2}>, #<Set: {0, 1}>, #<Set: {4, 0}
#<Set: {0, 5}>, #<Set: {5, 4}>, #<Set: {4, 2}>, #<Set: {0, 1}>, #<Set: {4, 0}>, #<Set: {0, 2}>, #<Set: {1, 5}>, #<Set: {3, 5}>, #<Set: {2, 1}
#<Set: {3, 5}>, #<Set: {2, 1}>, #<Set: {0, 5}>, #<Set: {5, 4}>, #<Set: {4, 2}>, #<Set: {0, 1}>, #<Set: {4, 0}>, #<Set: {0, 2}>, #<Set: {1, 5}
#<Set: {2, 3}>, #<Set: {1, 3}>}>
#<Set: {1, 3}>, #<Set: {2, 3}>}>
#<Set: {2, 1}>, #<Set: {0, 5}>, #<Set: {5, 4}>, #<Set: {4, 2}>, #<Set: {0, 1}>, #<Set: {4, 0}>, #<Set: {0, 2}>, #<Set: {1, 5}>, #<Set: {3, 5}
#<Set: {4, 3}>}>
#<Set: {0, 1}>, #<Set: {4, 0}>, #<Set: {0, 2}>, #<Set: {1, 5}>, #<Set: {3, 5}>, #<Set: {2, 1}>, #<Set: {0, 5}>, #<Set: {5, 4}>, #<Set: {4, 2}
#<Set: {4, 2}>, #<Set: {0, 1}>, #<Set: {4, 0}>, #<Set: {0, 2}>, #<Set: {1, 5}>, #<Set: {3, 5}>, #<Set: {2, 1}>, #<Set: {0, 5}>, #<Set: {5, 4}