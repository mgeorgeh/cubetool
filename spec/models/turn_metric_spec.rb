require "rails_helper"

RSpec.describe TurnMetric do
  describe "count_turns" do
    it "counts moves correctly" do
      sequence = MoveSequence.from_string("RD2R'UB")
      qtm = TurnMetric.new("QTM")
      htm = TurnMetric.new("HTM")
      expect(sequence.sum{|m| qtm.count_turns(m)}).to eql(6)
      expect(sequence.sum{|m| htm.count_turns(m)}).to eql(5)
    end
  end
end