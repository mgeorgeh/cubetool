require "rails_helper"

RSpec.describe Piece do
  describe "applying_move" do
    it "applies moves correctly" do
      c = Corner.from_arrs([0,2,1],[0,2,1])
      cr = Corner.from_arrs([0,1,5],[0,2,1])
      expect(c.applying_move(R)).to eq(cr)
      cri = Corner.from_arrs([0,4,2],[0,2,1])
      expect(c.applying_move(Ri)).to eq(cri)
      cr2 = Corner.from_arrs([0,5,4],[0,2,1])
      expect(c.applying_move(R2)).to eq(cr2)
      expect(c.applying_move(L)).to eq(c)
      cu = Corner.from_arrs([3,1,2],[2,1,0])
      expect(c.applying_move(U)).to eq(cu)
      e = Edge.from_arrs([0,1],[0,1])
      eu = Edge.from_arrs([1,2],[1,0])
      expect(e.applying_move(U)).to eq(eu) 
    end
  end
end