class AddTestCrossSolution < ActiveRecord::Migration[5.1]
  def change
    create_table :test_cross_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :test_cross_solutions, :state
  end
end
