class AddStateIndexToCrossSolutions < ActiveRecord::Migration[5.1]
  def change
    add_index :cross_solutions, :state
  end
end
