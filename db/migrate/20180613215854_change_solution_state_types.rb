class ChangeSolutionStateTypes < ActiveRecord::Migration[5.1]
  def change
    [:cross_solutions, :cep1_solutions, :cep2a_solutions].each do |tabl|
      change_column tabl, :state, :text
    end
  end
end
