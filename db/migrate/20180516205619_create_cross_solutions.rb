class CreateCrossSolutions < ActiveRecord::Migration[5.1]
  def change
    create_table :cross_solutions do |t|
      t.integer :state
      t.text :solution
    end
  end
end
