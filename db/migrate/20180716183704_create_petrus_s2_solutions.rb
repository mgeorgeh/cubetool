class CreatePetrusS2Solutions < ActiveRecord::Migration[5.2]
  def change
    create_table :petrus_s2_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :petrus_s2_solutions, :state
  end
end
