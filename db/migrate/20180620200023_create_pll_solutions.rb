class CreatePllSolutions < ActiveRecord::Migration[5.1]
  def change
    create_table :pll_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :pll_solutions, :state
  end
end
