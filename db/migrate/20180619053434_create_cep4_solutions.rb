class CreateCep4Solutions < ActiveRecord::Migration[5.1]
  def change
    create_table :cep4_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :cep4_solutions, :state
  end
end
