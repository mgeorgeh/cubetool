class CreatePetrusS4Solutions < ActiveRecord::Migration[5.2]
  def change
    create_table :petrus_s4_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :petrus_s4_solutions, :state
  end
end
