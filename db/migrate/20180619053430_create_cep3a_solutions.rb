class CreateCep3aSolutions < ActiveRecord::Migration[5.1]
  def change
    create_table :cep3a_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :cep3a_solutions, :state
  end
end
