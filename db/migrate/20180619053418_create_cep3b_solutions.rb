class CreateCep3bSolutions < ActiveRecord::Migration[5.1]
  def change
    create_table :cep3b_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :cep3b_solutions, :state
  end
end
