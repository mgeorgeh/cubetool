class CreateZbllSolutions < ActiveRecord::Migration[5.2]
  def change
    create_table :zbll_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :zbll_solutions, :state
  end
end
