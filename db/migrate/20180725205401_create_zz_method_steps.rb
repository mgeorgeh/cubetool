class CreateZzMethodSteps < ActiveRecord::Migration[5.2]
  def change
    [
      :zz_s2_solutions,
      :zz_s3a_solutions,
      :zz_s3b_solutions,
      :zz_s3c_solutions,
      :zz_s4a_solutions,
      :zz_s4b_solutions,
      :zz_s4c_solutions,
      :zz_s5_solutions,
    ].each do |table_name|
      create_table table_name do |t|
        t.text :state
        t.text :solution
      end
      add_index table_name, :state
    end
  end
end
