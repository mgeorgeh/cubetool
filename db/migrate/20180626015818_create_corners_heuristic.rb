class CreateCornersHeuristic < ActiveRecord::Migration[5.1]
  def change
    create_table :corners_heuristics do |t|
      t.text :state
      t.integer :depth
    end
  end
end
