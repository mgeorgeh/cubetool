class CreateCep2aSolutions < ActiveRecord::Migration[5.1]
  def change
    create_table :cep2a_solutions do |t|
      t.integer :state, limit: 8
      t.text :solution
    end
    add_index :cep2a_solutions, :state
  end
end
