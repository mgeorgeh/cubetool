class CreatePetrusS3Solutions < ActiveRecord::Migration[5.2]
  def change
    create_table :petrus_s3_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :petrus_s3_solutions, :state
  end
end
