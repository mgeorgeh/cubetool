class CreateOllSolutions < ActiveRecord::Migration[5.1]
  def change
    create_table :oll_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :oll_solutions, :state
  end
end
