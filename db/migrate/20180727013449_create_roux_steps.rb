class CreateRouxSteps < ActiveRecord::Migration[5.2]
  def change
    [
      :roux_s2_solutions,
      :roux_s3_solutions,
      :roux_s4_solutions,
      :roux_s5_solutions,
      :roux_s6_solutions,
      :roux_s7_solutions,
    ].each do |table_name|
      create_table table_name do |t|
        t.text :state
        t.text :solution
      end
      add_index table_name, :state
    end
  end
end
