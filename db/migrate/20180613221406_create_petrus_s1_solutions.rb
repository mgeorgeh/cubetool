class CreatePetrusS1Solutions < ActiveRecord::Migration[5.1]
  def change
    create_table :petrus_s1_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :petrus_s1_solutions, :state
  end
end
