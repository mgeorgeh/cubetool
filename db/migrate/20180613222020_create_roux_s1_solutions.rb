class CreateRouxS1Solutions < ActiveRecord::Migration[5.1]
  def change
    create_table :roux_s1_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :roux_s1_solutions, :state
  end
end
