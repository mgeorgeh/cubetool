class CreatePetrusS5Solutions < ActiveRecord::Migration[5.2]
  def change
    create_table :petrus_s5_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :petrus_s5_solutions, :state
  end
end
