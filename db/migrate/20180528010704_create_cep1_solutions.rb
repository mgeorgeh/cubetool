class CreateCep1Solutions < ActiveRecord::Migration[5.1]
  def change
    create_table :cep1_solutions do |t|
      t.integer :state, limit: 8
      t.text :solution
    end
    add_index :cep1_solutions, :state
  end
end
