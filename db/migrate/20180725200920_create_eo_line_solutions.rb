class CreateEoLineSolutions < ActiveRecord::Migration[5.2]
  def change
    create_table :eo_line_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :eo_line_solutions, :state
  end
end
