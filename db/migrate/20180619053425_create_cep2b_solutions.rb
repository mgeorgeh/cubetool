class CreateCep2bSolutions < ActiveRecord::Migration[5.1]
  def change
    create_table :cep2b_solutions do |t|
      t.text :state
      t.text :solution
    end
    add_index :cep2b_solutions, :state
  end
end
