# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_27_013449) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cep1_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_cep1_solutions_on_state"
  end

  create_table "cep2a_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_cep2a_solutions_on_state"
  end

  create_table "cep2b_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_cep2b_solutions_on_state"
  end

  create_table "cep3a_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_cep3a_solutions_on_state"
  end

  create_table "cep3b_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_cep3b_solutions_on_state"
  end

  create_table "cep4_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_cep4_solutions_on_state"
  end

  create_table "corners_heuristics", force: :cascade do |t|
    t.text "state"
    t.integer "depth"
  end

  create_table "cross_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_cross_solutions_on_state"
  end

  create_table "eo_line_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_eo_line_solutions_on_state"
  end

  create_table "oll_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_oll_solutions_on_state"
  end

  create_table "petrus_s1_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_petrus_s1_solutions_on_state"
  end

  create_table "petrus_s2_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_petrus_s2_solutions_on_state"
  end

  create_table "petrus_s3_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_petrus_s3_solutions_on_state"
  end

  create_table "petrus_s4_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_petrus_s4_solutions_on_state"
  end

  create_table "petrus_s5_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_petrus_s5_solutions_on_state"
  end

  create_table "pll_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_pll_solutions_on_state"
  end

  create_table "roux_s1_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_roux_s1_solutions_on_state"
  end

  create_table "roux_s2_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_roux_s2_solutions_on_state"
  end

  create_table "roux_s3_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_roux_s3_solutions_on_state"
  end

  create_table "roux_s4_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_roux_s4_solutions_on_state"
  end

  create_table "roux_s5_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_roux_s5_solutions_on_state"
  end

  create_table "roux_s6_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_roux_s6_solutions_on_state"
  end

  create_table "roux_s7_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_roux_s7_solutions_on_state"
  end

  create_table "test_cross_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_test_cross_solutions_on_state"
  end

  create_table "zbll_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zbll_solutions_on_state"
  end

  create_table "zz_s2_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zz_s2_solutions_on_state"
  end

  create_table "zz_s3a_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zz_s3a_solutions_on_state"
  end

  create_table "zz_s3b_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zz_s3b_solutions_on_state"
  end

  create_table "zz_s3c_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zz_s3c_solutions_on_state"
  end

  create_table "zz_s4a_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zz_s4a_solutions_on_state"
  end

  create_table "zz_s4b_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zz_s4b_solutions_on_state"
  end

  create_table "zz_s4c_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zz_s4c_solutions_on_state"
  end

  create_table "zz_s5_solutions", force: :cascade do |t|
    t.text "state"
    t.text "solution"
    t.index ["state"], name: "index_zz_s5_solutions_on_state"
  end

end
