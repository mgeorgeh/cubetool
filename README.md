# README

A training tool for competitive solvers of Rubik's cubes.

This tool generates optimal solutions to each substep of four common methods for solving a Rubik's cube.

## Example Screenshot

![](app/assets/images/cubetool_screen_1.png)

Along the top the user can select which method they would like to train. A scramble will then be generated and displayed below.

The current cube state is displayed below the scramble display and the solutions to the current step are displayed in the box on the right. The controls along the bottom, from left to right, are as follows:

1. Back and Forward buttons to allow the user to backtrack and try out different solutions.
2. A refresh button to generate a new scramble.
3. Controls to rotate the cube along each of the 3 spacial axes, depending on which angle the user prefers to view the cube from.

## Frontend Architecture

The bulk of the front end of this app is contained 3 JavaScript files located in the app/javascript/stuff bundle (In retrospect, a more descriptive name could have been chosen). drawing.js contains one function whose purpose is to render a cube state onto the html5 canvas object. transform.js contains functions for translating sequences of moves between different perspectives. This is necessary because Rubik's cube move notation is relative to the face that the solver is currently looking at.

As a descriptive example, the move sequence denoted `R U' F2` means "Rotate the **R**ight face 90 degrees clockwise, then rotate the **U**p face 90 degrees counterclockwise, then rotate the **F**ront face 180 degrees." The six faces are denoted **R**ight, **U**p, **F**ront, **L**eft, **D**own, **B**ack. In the above screenshot, the face with the white center is the front face. If the user were to rotate the cube, all solutions and the current scramble would need to be adjusted to match the user's new viewpoint.

The main file is index.js. It contains a single global variable called ```app```, which contains the entire state of the application at any given time. Any time the user performs an action, the state is modified, and the user interface is re-displayed relative to the new state. 





