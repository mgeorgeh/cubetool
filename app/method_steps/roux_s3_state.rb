class RouxS3State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['LD', 'LB', 'LF', 'LDF', 'LDB', 'RB', 'RD', 'RDB']
  end

  def self.pieces_to_solve
    ['RF', 'RDF']
  end

  def self.label
    ['RF']
  end
end