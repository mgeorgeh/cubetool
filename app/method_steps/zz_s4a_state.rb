class ZzS4aState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DF', 'DB', 'LD', 'LB', 'DLB', 'RB', 'RD', 'DRB']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'FR', 'FL']
  end

  def self.pieces_to_solve
    ['FL', 'DFL']
  end

  def self.label
    ['LF']
  end

  def self.varying_transforms
    [NullRotation, RLRef].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end