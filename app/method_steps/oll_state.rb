class OllState
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  # def self.included_pieces
  #   [
  #     'DF', 'DR', 'DB', 'DL',
  #     'DRF', 'FR', 'DFL', 'FL',
  #     'DRB', 'RB', 'DBL', 'BL',
  #     'UF', 'UR', 'UB', 'UL',
  #     'URF', 'URB', 'ULF', 'ULB',
  #   ]
  # end

  # def self.varying_pieces
  #   [
  #     'UF', 'UR', 'UB', 'UL',
  #     'URF', 'URB', 'ULF', 'ULB',
  #   ]
  # end

  def self.pieces_to_preserve
    [
      'DF', 'DR', 'DB', 'DL',
      'DRF', 'FR', 'DFL', 'FL',
      'DRB', 'RB', 'DBL', 'BL',
    ] 
  end

  def self.pieces_to_orient
    [
      'UF', 'UR', 'UB', 'UL',
      'URF', 'URB', 'ULF', 'ULB'
    ]
  end

  def self.label
    ['U']
  end

  def reachable?
    valid?
  end

  def self.pre_filter(piece)
    piece.permuted_correctly?
  end

  def to_acube_input
    pieces_str = [
      'UF', 'UR', 'UB', 'UL', 
      'DF', 'DR', 'DB', 'DL',
      'FR', 'FL', 'BR', 'BL',
      'UFR', 'URB', 'UBL', 'ULF',
      'DRF', 'DFL', 'DLB', 'DBR',
    ].map do |str| 
      p = piece_at(str)
      if p
        if p.coord("U")
          i = p.orientation_profile_int
          if p.is_a? Edge
            {
              0 => "?",
              1 => "-?"
            }[i]
          else
            {
              0 => "?",
              1 => "-?",
              2 => "+?"
            }[i]
          end
        else 
          str.chars.map do |c|
            self.class.int_to_char_sticker_lookup(p.face(c))
          end.join
        end
      else
        "@?"
      end
    end.join(" ")
    rest = "\n3: R* U* F* L* D* B*\n4: ftm\n5: 20\n6: yes\n7: yes\n\n"
    "1: " + pieces_str + rest
  end
end