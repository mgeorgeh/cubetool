class ZzS3bState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DF', 'DB', 'DL', 'BL', 'BDL']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'FL', 'RB', 'FR', 'DR']
  end

  def self.pieces_to_solve
    ['DR', 'FR', 'DFR']
  end

  def self.label
    ['FR']
  end
end