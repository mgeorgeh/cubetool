class RouxS2State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['LF', 'LD', 'LB', 'LDF', 'LBD']
  end

  def self.pieces_to_solve
    ['RD', 'RB', 'RDB']
  end

  def self.label
    ['RB']
  end

  def self.varying_transforms
    [NullRotation, FBRef].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end