class ZzS4cState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DF', 'DB', 'LD', 'LB', 'DLB', 'LF', 'DLF']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'FR', 'RB', 'RD']
  end

  def self.pieces_to_solve
    ['RB', 'RD', 'DRB']
  end

  def self.label
    ['RB']
  end

  def self.varying_transforms
    [NullRotation, FBRef].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end