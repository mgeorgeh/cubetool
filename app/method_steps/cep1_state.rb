# Cep = Corner-edge pair
class Cep1State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  # def self.included_pieces 
  #   ['DR', 'DB', 'DL', 'DF', 'FR', 'DRF']
  # end

  # def self.varying_pieces
  #   ['FR', 'DRF']
  # end

  def self.pieces_to_preserve
    ['DF', 'DR', 'DB', 'DL']
  end

  def self.pieces_to_solve
    ['FR', 'DRF']
  end

  def self.label
    ['FR']
  end

  def self.varying_transforms
    [NullRotation, Y, Yi, Y2].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end