class RouxS6State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['LD', 'LB', 'LF', 'LDF', 'LDB', 'RD', 'RF', 'RB', 'RDF', 'RDB', 'URF', 'URB', 'ULF', 'ULB']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'DF', 'DB']
  end

  def self.pieces_to_solve
    ['UL', 'UR']
  end

  def self.label
    ['UL', 'UR']
  end
end