class ZzS4bState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DF', 'DB', 'LD', 'LB', 'DLB', 'DR', 'RF', 'DRF']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'RB', 'FL']
  end

  def self.pieces_to_solve
    ['FL', 'DFL']
  end

  def self.label
    ['FL']
  end

  def self.varying_transforms
    [NullRotation, Y2].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end