class ZzS3aState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DF', 'DB', 'DL', 'BL', 'BDL']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'FR', 'FL', 'DR', 'RB']
  end

  def self.pieces_to_solve
    ['DR', 'RB', 'DRB']
  end

  def self.label
    ['RB']
  end
end