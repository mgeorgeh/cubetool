class RouxS7State
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  def self.pieces_to_preserve
    ['LD', 'LB', 'LF', 'LDF', 'LDB', 'RD', 'RF', 'RB', 'RDF', 'RDB', 'URF', 'URB', 'ULF', 'ULB', 'UL', 'UR']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UB', 'DF', 'DB']
  end

  def self.pieces_to_solve
    ['UF', 'UB', 'DF', 'DB']
  end

  def self.label
    ['U']
  end

  def reachable?
    valid?
  end
end