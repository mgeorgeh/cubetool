class PetrusS5State
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  def self.pieces_to_preserve
    ['DLB', 'DL', 'DB', 'LB', 'LF', 'DLF', 'DF', 'RB', 'RD', 'DRB']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'RF']
  end

  def self.pieces_to_solve
    ['RF', 'DRF']
  end

  def self.label
    ['RF']
  end
end