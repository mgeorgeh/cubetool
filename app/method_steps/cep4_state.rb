class Cep4State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  # def self.included_pieces
  #   [
  #     'DF', 'DR', 'DB', 'DL', 
  #     'FR', 'DRF', 'RB', 'DRB', 
  #     'BL', 'DBL', 'FL', 'DFL'
  #   ]
  # end

  # def self.varying_pieces
  #   ['RB', 'DRB']
  # end

  def self.pieces_to_preserve
    ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF', 'BL', 'DBL', 'FL', 'DFL']
  end

  def self.pieces_to_solve
    ['RB', 'DRB']
  end

  def self.label
    ['RB']
  end
end