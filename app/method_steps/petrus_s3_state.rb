class PetrusS3State
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  def self.pieces_to_preserve
    ['DLB', 'DL', 'DB', 'LB', 'LF', 'DLF', 'DF']
  end

  def self.pieces_to_orient
    ['UF', 'UR', 'UB', 'UL', 'RF', 'RD', 'RB']
  end

  def self.label
    ["U", "B"]
  end

  def reachable?
    edges.valid?
  end
end
