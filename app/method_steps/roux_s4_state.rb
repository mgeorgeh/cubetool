class RouxS4State
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  def self.pieces_to_preserve
    ['LD', 'LB', 'LF', 'LDF', 'LDB', 'RD', 'RB', 'RF', 'RDF', 'RDB']
  end

  def self.pieces_to_solve
    ['URF', 'URB', 'ULB', 'ULF']
  end

  def self.label
    ['U']
  end

  def reachable?
    corners.valid?
  end
end