class EoLineState
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  #    UF UR UB UL FR FL BR BL DF DR DB DL     D   F   D   B
  #    01_01_01_00_01_01_01_01_01_00_00_00 || 011_101_101_001
  # UR:00_11_00_00_00_00_00_00_00_00_00_00_000_000_000_000
  # BR:00_00_00_00_00_00_11_00_00_00_00_00_000_000_000_000
  # DR:00_00_00_00_00_00_00_00_00_11_00_00_000_000_000_000
  # FR:00_00_00_00_11_00_00_00_00_00_00_00_000_000_000_000
  # 00_00_00_00_00_00_00_00_00_00_00_00_000_000_000_000
  # 
  def self.pieces_to_orient
    ['UF', 'UR', 'UB', 'UL', 'FR', 'FL', 'BR', 'BL', 'DF', 'DR', 'DB', 'DL']    
  end

  def self.pieces_to_solve
    ['DF', 'DB']
  end

  def self.label
    ['DF', 'DB']
  end

  def self.varying_transforms
    [
      [NullRotation], [Y], [Z], [Z, Y], 
      [Zi], [Zi, Yi], [X], [X, Z],
      [Xi], [Xi, Z], [X2], [X2, Y],
    ].map do |t|
      TransformationSequence.from_move_list(t)
    end
  end

  def self.apply_move(m, state)
    h = @@move_lookup_hash[m]
    tc = @@this_coord_hash[m]

    permutation_arr = @@permutation_hash[m]

    # UF UR UB UL FR FL BR BL DF DR DB DL     D   F   D   B
    if m == "F" || m == 'Fi'
      state = state ^ 0b01_00_00_00_01_01_00_00_01_00_00_00_000_000_000_000
    elsif m == "B" || m == "Bi"
      state = state ^ 0b00_00_01_00_00_00_01_01_00_00_01_00_000_000_000_000
    end

    s0 = permutation_arr[0]
    s1 = permutation_arr[1]
    s2 = permutation_arr[2]
    s3 = permutation_arr[3]
    if m == "R2" || m == "U2" || m == "F2" || m == "L2" || m == "D2" || m == "B2"
      xor = ((state >> s0) & 3) ^ ((state >> s1) & 3)
      xor = (xor << s0) | (xor << s1)
      state = state ^ xor

      xor = ((state >> s2) & 3) ^ ((state >> s3) & 3)
      xor = (xor << s2) | (xor << s3)
      state = state ^ xor
    else
      xor = ((state >> s0) & 3) ^ ((state >> s1) & 3)
      xor = (xor << s0) | (xor << s1)
      state = state ^ xor

      xor = ((state >> s1) & 3) ^ ((state >> s2) & 3)
      xor = (xor << s1) | (xor << s2)
      state = state ^ xor

      xor = ((state >> s2) & 3) ^ ((state >> s3) & 3)
      xor = (xor << s2) | (xor << s3)
      state = state ^ xor
    end
       
    sc = (state & 0b00_00_00_00_00_00_00_00_00_00_00_00_111_000_000_000) >> 9 
    sd = (state & 0b00_00_00_00_00_00_00_00_00_00_00_00_000_111_000_000) >> 6 

    if sc == tc || sd == tc
      sc = h[sc]
      sd = h[sd]
    end

    se = (state & 0b00_00_00_00_00_00_00_00_00_00_00_00_000_000_111_000) >> 3 
    sf = (state & 0b00_00_00_00_00_00_00_00_00_00_00_00_000_000_000_111) 

    if se == tc || sf == tc
      se = h[se]
      sf = h[sf]
    end

    (state & 0b11_11_11_11_11_11_11_11_11_11_11_11_000_000_000_000) | (sf | (se << 3) | (sd << 6) | (sc << 9))
  end

  # solution generation is different enough as to not be easily abstracted
  def self.generate_solutions
    @@prev = Set.new
    @@ssi = StateIntEncoding.new(solved_state).int
    @@current = Set.new([@@ssi])
    @@nexts = Set.new
    @@result = {}
    @@moves = [
      "R", "Ri", "R2", 
      "U", "Ui", "U2", 
      "F", "Fi", "F2", 
      "L", "Li", "L2", 
      "D", "Di", "D2", 
      "B", "Bi", "B2",
    ]

    @@this_coord_hash = {
      "R"  => 0,
      "Ri" => 0,
      "R2" => 0,
      "U"  => 1,
      "Ui" => 1,
      "U2" => 1,
      "F"  => 2,
      "Fi" => 2,
      "F2" => 2,
      "L"  => 3,
      "Li" => 3,
      "L2" => 3,
      "D"  => 4,
      "Di" => 4,
      "D2" => 4,
      "B"  => 5,
      "Bi" => 5,
      "B2" => 5,
    }

    @@move_lookup_hash = {
      "R"  => R.cycle_hash,
      "Ri" => Ri.cycle_hash,
      "R2" => R2.cycle_hash,
      "U"  => U.cycle_hash,
      "Ui" => Ui.cycle_hash,
      "U2" => U2.cycle_hash,
      "F"  => F.cycle_hash,
      "Fi" => Fi.cycle_hash,
      "F2" => F2.cycle_hash,
      "L"  => L.cycle_hash,
      "Li" => Li.cycle_hash,
      "L2" => L2.cycle_hash,
      "D"  => D.cycle_hash,
      "Di" => Di.cycle_hash,
      "D2" => D2.cycle_hash,
      "B"  => B.cycle_hash,
      "Bi" => Bi.cycle_hash,
      "B2" => B2.cycle_hash,
    } 

    @@permutation_hash = {
      "R"  => [16,22,32,26],
      "Ri" => [16,26,32,22],
      "R2" => [16,32,22,26],
      "U"  => [28,34,32,30],
      "Ui" => [28,30,32,34],
      "U2" => [28,32,30,34],
      "F"  => [18,26,34,24],
      "Fi" => [18,24,34,26],
      "F2" => [18,34,26,24],
      "L"  => [12,24,28,20],
      "Li" => [12,20,28,24],
      "L2" => [12,28,20,24],
      "D"  => [12,14,16,18],
      "Di" => [12,18,16,14],
      "D2" => [12,16,18,14],
      "B"  => [14,20,30,22],
      "Bi" => [14,22,30,20],
      "B2" => [14,30,22,20],
    }

    @@inverse_hash = {
      "R"  => R.inverse.to_s,
      "Ri" => Ri.inverse.to_s,
      "R2" => R2.inverse.to_s,
      "U"  => U.inverse.to_s,
      "Ui" => Ui.inverse.to_s,
      "U2" => U2.inverse.to_s,
      "F"  => F.inverse.to_s,
      "Fi" => Fi.inverse.to_s,
      "F2" => F2.inverse.to_s,
      "L"  => L.inverse.to_s,
      "Li" => Li.inverse.to_s,
      "L2" => L2.inverse.to_s,
      "D"  => D.inverse.to_s,
      "Di" => Di.inverse.to_s,
      "D2" => D2.inverse.to_s,
      "B"  => B.inverse.to_s,
      "Bi" => Bi.inverse.to_s,
      "B2" => B2.inverse.to_s,
    } 

    depth = 0
    loop do
      puts "depth: #{depth}, prev: #{@@prev.count}, curr: #{@@current.count}, res: #{@@result.count}"
      @@current.each do |state|
        @@moves.each do |m|
          next_state = apply_move(m, state)

          if @@current.include?(next_state) || @@prev.include?(next_state)
            next
          else
            @@nexts.add(next_state)
            (@@result[next_state] || @@result[next_state] = []) << @@inverse_hash[m]
          end
        end
      end
      if @@nexts.empty?
        break
      end
      @@prev = @@current
      @@current = @@nexts
      @@nexts = Set.new
      depth += 1
    end
    puts "gen done. found #{@@result.count} items"

    vals = ""
    i = 0
    @@result.each do |state, _|
      s = solutions(state)
      s.each do |sol|
        vals << "('#{state}', '#{sol}'), "
      end

      if i == 1000
        vals.chop!
        vals.chop!
        do_database_stuff(vals)
        vals = ""
        i = 0
      end
      i += 1
    end

    vals.chop!
    vals.chop!
    do_database_stuff(vals)

    puts "%d MB" % (`ps -o rss= -p #{Process.pid}`.to_i/1024)
  end

  def self.do_database_stuff(vals)
    solution_type.transaction do
      cols = solution_type.column_names.drop(1).join(", ")
      solution_type.connection.exec_query "INSERT INTO #{solution_table_name} (#{cols}) VALUES #{vals}" 
    end
  end

  def self.solutions(state_int)
    result = []
    frontier = [[state_int, ""]]
    while frontier.length > 0
      state, sol = frontier.shift
      if state == @@ssi
        result << sol
        next
      end
      next_moves = @@result[state]
      next_moves.each do |m|
        frontier << [apply_move(m, state), sol + m]
      end
    end
    result
  end
end
