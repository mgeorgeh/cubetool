class PllState
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  def self.pieces_to_preserve
    [
      'DF', 'DR', 'DB', 'DL',
      'DRF', 'FR', 'DRB', 'RB',
      'DFL', 'FL', 'DLB', 'LB',
    ]
  end

  def self.pieces_to_preserve_orientation
    [
      'URF', 'UF', 'ULF', 'UL',
      'ULB', 'UB', 'URB', 'UR',
    ]
  end

  def self.pieces_to_solve
    [
      'URF', 'UF', 'ULF', 'UL',
      'ULB', 'UB', 'URB', 'UR',
    ]
  end


  def self.label
    ['U']
  end

  def reachable?
    valid?
  end
end