class ZzS5State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DF', 'DB', 'LD', 'LB', 'DLB', 'LF', 'DFL', 'RD', 'RB', 'DRB']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'FR']
  end

  def self.pieces_to_solve
    ['FR', 'DFR']
  end

  def self.label
    ['FR']
  end
end