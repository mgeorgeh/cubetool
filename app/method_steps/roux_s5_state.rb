class RouxS5State
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  def self.pieces_to_preserve
    ['LD', 'LB', 'LF', 'LDF', 'LDB', 'RD', 'RF', 'RB', 'RDF', 'RDB', 'URF', 'URB', 'ULF', 'ULB']
  end

  def self.pieces_to_orient
    ['UF', 'UR', 'UB', 'UL', 'DF', 'DB']
  end

  def self.label
    ['U']
  end

  def reachable?
    edges.valid?
  end
end