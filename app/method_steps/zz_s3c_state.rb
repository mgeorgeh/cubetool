class ZzS3cState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DF', 'DB', 'DL', 'BL', 'BDL']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'RD', 'RB', 'RF', 'LF']
  end

  def self.pieces_to_solve
    ['LF', 'DFL']
  end

  def self.label
    ['LF']
  end
end