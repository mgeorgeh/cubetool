class ZbllState
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation

  def self.pieces_to_preserve
    ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF', 'FL', 'DLF', 'RB', 'DRB', 'LB', 'DLB']
  end

  def self.pieces_to_solve
    ['UF', 'UR', 'UB', 'UL', 'URF', 'URB', 'ULF', 'ULB']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL']
  end

  def self.label
    ['U']
  end

  def reachable?
    valid?
  end
end