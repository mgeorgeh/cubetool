class Cep2bState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  # def self.included_pieces
  #   ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF', 'LB', 'DLB']
  # end

  # def self.varying_pieces
  #   ['LB', 'DLB']
  # end

  def self.pieces_to_preserve
    ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF']
  end

  def self.pieces_to_solve
    ['LB', 'DLB']
  end

  def self.label
    ['LB']
  end
end