class RouxS1State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_solve
    ['LF', 'LD', 'LB', 'LDF', 'LBD']
  end

  def self.label
    ['LDF', 'LDB']
  end

  def self.varying_transforms
    [
      [NullRotation],[X], [Xi], [X2], 
      [Y], [Y, Z], [Y, Zi], [Y, Z2],
      [Y2], [Y2, X], [Y2, Xi], [Y2, X2], 
      [Yi], [Yi, Z], [Yi, Zi], [Yi, Z2], 
      [Z], [Z, Y], [Z, Yi], [Z, Y2],
      [Zi], [Zi, Y], [Zi, Yi], [Zi, Y2],
    ].map do |t|
      TransformationSequence.from_move_list(t)
    end
  end
end