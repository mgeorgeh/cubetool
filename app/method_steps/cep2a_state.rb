class Cep2aState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  # def self.included_pieces
  #   ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF', 'FL', 'DFL']
  # end

  # def self.varying_pieces
  #   ['FL', 'DFL']
  # end

  def self.pieces_to_preserve
    ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF']
  end

  def self.pieces_to_solve
    ['FL', 'DFL']
  end

  def self.label
    ['FL']
  end

  def self.varying_transforms
    [[NullRotation], [Y, RLRef]].map do |ts|
      TransformationSequence.from_move_list(ts)
    end
  end
end