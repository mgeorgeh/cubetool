require 'pp'
class CrossState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  # def self.included_pieces
  #   ['DF', 'DR', 'DB', 'DL']
  # end

  def self.pieces_to_solve
    ['DF', 'DR', 'DB', 'DL']
  end

  def self.label
    ["D"]
  end

  def self.varying_transforms
    [NullRotation, X, Xi, X2, Z, Zi].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end