class PetrusS4State
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include Validation
  
  def self.pieces_to_preserve
    ['DLB', 'DL', 'DB', 'LB', 'LF', 'DLF', 'DF']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'RF', 'RD', 'RB']
  end

  def self.pieces_to_solve
    ['RB', 'RD', 'DRB']
  end

  def self.label
    ['DRB']
  end

  def self.varying_transforms
    [NullRotation, FBRef].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end