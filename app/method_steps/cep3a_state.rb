class Cep3aState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  # def self.included_pieces
  #   ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF', 'FL', 'DFL', 'LB', 'DLB']
  # end

  # def self.varying_pieces
  #   ['LB', 'DLB']
  # end

  def self.pieces_to_preserve
    ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF', 'FL', 'DFL']
  end

  def self.pieces_to_solve
    ['LB', 'DLB']
  end

  def self.label
    ['LB']
  end

  def self.varying_transforms
    [NullRotation, RLRef].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end