class PetrusS1State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_solve
    ['DLB', 'LB', 'DL', 'DB']
  end

  def self.label
    ['DLB']
  end

  def self.varying_transforms
    [[NullRotation], [Y], [Yi], [Y2], [Xi], [X2], [X2, Yi], [Z2]].map do |t|
      TransformationSequence.from_move_list(t)
    end
  end
end