class PetrusS2State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DLB', 'LB', 'DL', 'DB']
  end

  def self.pieces_to_solve
    ['LF', 'DF', 'DLF']
  end

  def self.label
    ['DLF']
  end

  def self.varying_transforms
    [[NullRotation], [Xi, Zi], [X, Y]].map do |t|
      TransformationSequence.from_move_list(t)
    end
  end
end