class CornersState
  attr_accessor :pieces
  include CubeSection
  include MethodStep
  include HorribleHackBuilder
  
  def self.included_pieces
    # ['DRF', 'DLF', 'DRB', 'DLB', 'URF', 'ULF', 'URB', 'ULB']
    ['DRF', 'DLF', 'DRB', 'DLB', 'URF', 'ULF']
  end
end