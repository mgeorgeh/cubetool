class Cep3bState
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  # def self.included_pieces
  #   ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF', 'LB', 'DLB', 'FL', 'DFL']
  # end

  # def self.varying_pieces
  #   ['FL', 'DFL']
  # end

  def self.pieces_to_preserve
    ['DF', 'DR', 'DB', 'DL', 'FR', 'DRF', 'LB', 'DLB']
  end

  def self.pieces_to_solve
    ['FL', 'DFL']
  end

  def self.label
    ['FL']
  end

  def self.varying_transforms
    [NullRotation, Y2].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end