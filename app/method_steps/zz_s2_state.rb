class ZzS2State
  attr_accessor :pieces
  include CubeSection
  include MethodStep

  def self.pieces_to_preserve
    ['DF', 'DB']
  end

  def self.pieces_to_preserve_orientation
    ['UF', 'UR', 'UB', 'UL', 'FR', 'FL', 'BR', 'BL', 'DR', 'DL']    
  end

  def self.pieces_to_solve
    ['BL', 'DL', 'BDL']
  end

  def self.label
    ['BL']
  end

  def self.varying_transforms
    [NullRotation, RLRef, FBRef, Y2].map do |t|
      TransformationSequence.from_move_list([t])
    end
  end
end