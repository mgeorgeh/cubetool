class EdgeSet < PieceSet

  def self.included_pieces
    ['UR', 'UB', 'UL', 'UF', 'FR', 'RB', 'BL', 'LF', 'DR', 'DB', 'DL', 'DF']
  end

  def piece_type
    Edge
  end

  def orientation_parity?
    @pieces.values.sum(&:flips_to_orient).even?
  end
end
