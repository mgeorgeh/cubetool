class CornerSet < PieceSet
  attr_accessor :pieces

  def self.included_pieces
    ['DFR', 'DRB', 'DBL', 'DLF', 'URF', 'UBR', 'ULB', 'UFL']
  end
  # private methods:
  def piece_type
    Corner
  end

  def orientation_parity?
    (@pieces.values.sum(&:clockwise_twists_to_orient) % 3).zero?
  end
end
