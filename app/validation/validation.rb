module Validation

  def valid?
    types_and_values_correct? && corners.valid? && edges.valid? && permutation_parity?
  end

  def corners
    CornerSet.new(@pieces.select { |_, p| p.is_a? Corner })
  end

  def edges
    EdgeSet.new(@pieces.select { |_, p| p.is_a? Edge })
  end

  def types_and_values_correct?
    @types_and_values_correct ||= true
  end

  def permutation_parity?
    (edges.swap_count + corners.swap_count).even?
  end
end

# will I ever need to validate somethign that is not a whole cube state?
# edge orientation: only even number of flips
# roux CLLs. I will only need to validate a complete cornerset or a complete
# edgeset
# do this for now, then split into CornerValidation and EdgeValidation later