class PieceSet
  attr_accessor :pieces
  include CubeSection

  def swap_count
    @pieces.values
           .map { |p| pieces_in_cycle(p) }
           .uniq
           .sum { |c| c.count - 1 }
  end

  def valid?
    all_pieces_present? && orientation_parity?
  end

  # private methods:

  def all_coords
    piece_type.valid_coords
  end

  def all_pieces_present?
    all_coords == Set.new(@pieces.values.map(&:faces))
  end

  def orientation_parity?
    raise NotImplementedError
  end

  def piece_type
    Piece
  end

  def pieces_in_cycle(piece)
    c = Set.new
    until c.include?(piece.faces)
      c.add(piece.faces)
      piece = @pieces.find { |_, p| p.coords == piece.faces }[1]
    end
    c
  end
end
