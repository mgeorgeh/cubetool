# Searches the state space for solutions to a cube state
# currently unused. replaced with acube.
class BidirectionalSearch
  def initialize(state_type)
    @state_type = state_type
    # sts = state_type.possible_states
    sts = PossibleStatesGenerator.new(state_type).states
    # @relevant_states = state_type.possible_states.map(&:to_hack_int)
    # @relevant_states = [sts[0].to_hack_int]
    @relevant_states = sts.map { |s| CompleteStateIntEncoding.new(s).int }
    @solution_type = state_type.solution_type
    @solution_table_name = state_type.solution_table_name
    # @solved_state_int = state_type.solved_hack_state_int
    @solved_state_int = CompleteStateIntEncoding.new(state_type.solved_state).int
    @big_result = {}
    @moves = [
      "R", "Ri", "R2", 
      "U", "Ui", "U2", 
      "F", "Fi", "F2", 
      "L", "Li", "L2", 
      "D", "Di", "D2", 
      "B", "Bi", "B2",
    ]
    @move_lookup_hash = {
      "R"  => R.cycle_hash,
      "Ri" => Ri.cycle_hash,
      "R2" => R2.cycle_hash,
      "U"  => U.cycle_hash,
      "Ui" => Ui.cycle_hash,
      "U2" => U2.cycle_hash,
      "F"  => F.cycle_hash,
      "Fi" => Fi.cycle_hash,
      "F2" => F2.cycle_hash,
      "L"  => L.cycle_hash,
      "Li" => Li.cycle_hash,
      "L2" => L2.cycle_hash,
      "D"  => D.cycle_hash,
      "Di" => Di.cycle_hash,
      "D2" => D2.cycle_hash,
      "B"  => B.cycle_hash,
      "Bi" => Bi.cycle_hash,
      "B2" => B2.cycle_hash,
    } 

    @this_coord_hash = {
      "R"  => 0,
      "Ri" => 0,
      "R2" => 0,
      "U"  => 1,
      "Ui" => 1,
      "U2" => 1,
      "F"  => 2,
      "Fi" => 2,
      "F2" => 2,
      "L"  => 3,
      "Li" => 3,
      "L2" => 3,
      "D"  => 4,
      "Di" => 4,
      "D2" => 4,
      "B"  => 5,
      "Bi" => 5,
      "B2" => 5,
    }

    @inverse_hash = {
      "R"  => R.inverse.to_s,
      "Ri" => Ri.inverse.to_s,
      "R2" => R2.inverse.to_s,
      "U"  => U.inverse.to_s,
      "Ui" => Ui.inverse.to_s,
      "U2" => U2.inverse.to_s,
      "F"  => F.inverse.to_s,
      "Fi" => Fi.inverse.to_s,
      "F2" => F2.inverse.to_s,
      "L"  => L.inverse.to_s,
      "Li" => Li.inverse.to_s,
      "L2" => L2.inverse.to_s,
      "D"  => D.inverse.to_s,
      "Di" => Di.inverse.to_s,
      "D2" => D2.inverse.to_s,
      "B"  => B.inverse.to_s,
      "Bi" => Bi.inverse.to_s,
      "B2" => B2.inverse.to_s,
    } 
    move_string = CompleteMoveApplicationMethod.new(state_type).string
    instance_eval(move_string)
  end

  def create_method(name, &block)
    self.class.send(:define_method, name, &block)
  end

  def step(direction)
    if direction == "forward"
      prev = @forward_prev
      current = @forward_current
      nexts = @forward_nexts
      result = @forward_result
    else
      prev = @backward_prev
      current = @backward_current
      nexts = @backward_nexts
      result = @backward_result
    end

    current.each do |state|
      @moves.each do |m|
        next_state = apply_move(m, state)
        if current.include?(next_state) || prev.include?(next_state)
          next
        else
          nexts.add(next_state)
          (result[next_state] || result[next_state] = []) << @inverse_hash[m]
        end
      end
    end

    if direction == "forward"
      @forward_prev = current
      @forward_current = nexts
      @forward_nexts = Set.new
      @forward_result = result
    else
      @backward_prev = current
      @backward_current = nexts
      @backward_nexts = Set.new
      @backward_result = result
    end
  end

  def done?
    !(@forward_result.keys & @backward_result.keys).empty?
  end

  def reset
    @forward_prev = Set.new
    @forward_current = [@solved_state_int]
    @forward_nexts = Set.new

    @backward_prev = Set.new
    @backward_current = [@goal_state]
    @backward_nexts = Set.new

    @forward_result = {}
    @backward_result = {}
  end

  def generate_solutions
    reset
    depth = 0
    direction = "forward"
    while !done?
      puts "depth: #{depth}, fc: #{@forward_current.count}, bc: #{@backward_current.count}"
      step(direction)
      direction = (direction == "forward") ? "backward" : "forward"
      depth += 1
      GC.start(full_mark: true, immediate_sweep: true)
      puts "%d MB" % (`ps -o rss= -p #{Process.pid}`.to_i/1024)
    end
    
    inter = @forward_result.keys & @backward_result.keys
    solutions = inter.flat_map do |st|
      forward_sols = half_solutions(st, @forward_result, @solved_state_int)
      backward_sols = half_solutions(st, @backward_result, @goal_state).map do |sol|
        MoveSequence.from_string(sol).inverse.to_s
      end
      result = []
      backward_sols.each do |bs|
        forward_sols.each do |fs|
          result << bs + fs
        end
      end
      result
    end

    @big_result[@goal_state] = solutions
    puts "%d MB" % (`ps -o rss= -p #{Process.pid}`.to_i/1024)
  end

  def half_solutions(state_int, result, end_point)
    solutions = []
    frontier = [[state_int, ""]]
    while frontier.length > 0
      state, sol = frontier.shift
      if state == end_point
        solutions << sol
        next
      end
      next_moves = result[state]
      next_moves.each do |m|
        frontier << [apply_move(m, state), sol + m]
      end
    end
    solutions
  end

  def run
    @relevant_states.each do |s|
      @goal_state = s
      generate_solutions
      puts @big_result.count
    end
    vals = ""
    @big_result.keys.each do |state|
      @big_result[state].each do |sol|
        vals << "('#{state}', '#{sol.tr("'", "i")}'), "
      end
    end
    vals.chop!
    vals.chop!

    puts "start database stuff"
    do_database_stuff(vals)
    # pp @big_result
  end

  def do_database_stuff(vals)
    # sleep(0.2)
    @solution_type.transaction do
      cols = @solution_type.column_names.drop(1).join(", ")
      @solution_type.connection.exec_query "INSERT INTO #{@solution_table_name} (#{cols}) VALUES #{vals}"
    end
  end
end