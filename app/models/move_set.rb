class MoveSet
  include Enumerable
  # def_delegators to remove 'each' def

  attr_writer :allowed_moves

  def initialize(str)
    str = 'RUFLDB' if str == 'ALL'
    @str = str
  end

  def self.unit_length_moves(metric)
    s = MoveSet.new('')
    s.allowed_moves = Set.new(all_moves.select { |m| metric.count_turns(m) == 1 })
    s
  end

  def self.all_moves
    [
      'R', 'Ri', 'R2',
      'L', 'Li', 'L2',
      'U', 'Ui', 'U2',
      'D', 'Di', 'D2',
      'F', 'Fi', 'F2',
      'B', 'Bi', 'B2'
    ]
  end

  def valid?
    underlying_sequence.valid?
  end

  def each
    allowed_moves.each do |m|
      yield m
    end
  end

  # private methods:

  def underlying_sequence
    @underlying_sequence ||= MoveSequence.from_string(@str)
  end

  def allowed_moves
    @allowed_moves ||= Set.new(underlying_sequence)
  end
end
