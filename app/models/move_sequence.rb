# movelist : [Move]
# from_string
# from_move_list

class MoveSequence < TransformationSequence
  include Enumerable

  attr_accessor :str

  def self.from_string(str)
    seq = MoveSequence.new
    seq.str = str.tr("i", "'").tr(" ", "")
    seq
  end

  def valid?
    @str.is_a?(String) && string_encodes_valid_moves?
  end

  def moves
    @moves ||= calc_moves
  end

  def compact
    # implement
  end

  def to_s
    s = moves.join(" ").tr('i', "'")
    s.empty? ? " " : s
  end

  # private methods:

  def string_encodes_valid_moves?
    !(@str =~ /\A([RUFLDBxyz]['2]?)+\z/).nil?
  end

  def calc_moves
    result = []
    chars = str.chars
    i = 0
    while i < chars.length
      move = chars[i]
      if ["'", "2"].include?(chars[i + 1])
        move << chars[i + 1]
        i += 2
      else
        i += 1
      end
      result << Move.from_string(move)
    end
    result
  end
end
