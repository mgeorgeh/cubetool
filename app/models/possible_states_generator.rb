class PossibleStatesGenerator
  def initialize(state_type)
    @state_type = state_type
  end

  def pieces_to_solve
    @state_type.pieces_to_solve
  end

  def pieces_to_preserve_orientation
    @state_type.pieces_to_preserve_orientation
  end

  def pieces_to_orient
    @state_type.pieces_to_orient
  end

  def pieces_to_preserve
    @state_type.pieces_to_preserve
  end

  def solved_core
    @solved_core ||= pieces_to_preserve.map { |s| Piece.solved(s) }
  end

  def occupied_locations
    @occupied_locations ||= solved_core.map(&:coords)
  end

  def arbitrarily_varying
    @arbitrarily_varying ||= ((pieces_to_solve - pieces_to_preserve_orientation).map do |s|
      Piece.solved(s)
        .valid_instances
        .select { |p| !occupied_locations.include?(p.coords) }
    end)
  end

  def permutation_varying 
    @permutation_varying ||= ((pieces_to_solve & pieces_to_preserve_orientation).map do |s|
      Piece.solved(s)
        .valid_instances
        .select { |p| p.correctly_oriented? && !occupied_locations.include?(p.coords) }
    end)
  end

  def orientation_varying
    @orientation_varying ||= (pieces_to_orient.map do |s|
      Piece.solved(s)
        .valid_instances
        .select { |p| p.permuted_correctly? && !occupied_locations.include?(p.coords) }
    end)
  end

  def possible_pieces
    @possible_pieces ||= (arbitrarily_varying + 
      permutation_varying + 
      orientation_varying)
  end

  def varying_pieces
    @varying_pieces ||= pieces_to_solve + pieces_to_orient
  end

  def included_pieces
    @included_pieces ||= pieces_to_preserve + varying_pieces
  end

  def states
    result = []
    counter = MultiBaseCounter.new(possible_pieces.map(&:length))
    loop do
      vals = counter.values
      pcs = vals.map.with_index { |i, j| possible_pieces[j][i] }
      const = pieces_to_preserve.zip(solved_core)
      var = varying_pieces.zip(pcs)
      st = @state_type.new((const + var).to_h)
      unique_pieces = Set.new(st.pieces.values.map(&:coords)).length
      if unique_pieces == included_pieces.length && st.reachable?
        result << st
      end
      counter.inc
      if counter.zero?
        break
      end
    end
    result
  end
end