class MethodStepExtractor
  def initialize(state_type:, cube_state:)
    @state_type = state_type
    @cube_state = cube_state
  end

  def extract
    @state_type.new(@state_type.all_pieces.map {|s| [s, @cube_state.piece(s)] }.to_h)
  end
end