# Piece with 3 stickers
class Corner < Piece
  attr_accessor :data
  def self.valid_coords
    Set[
      Set[0, 2, 1], Set[0, 1, 5], Set[0, 4, 2], Set[0, 5, 4],
      Set[3, 1, 2], Set[3, 5, 1], Set[3, 2, 4], Set[3, 4, 5]
    ]
  end

  def rotated_clockwise
    if something.include?(coords)
      return Corner.from_arrs([ud_coord, fb_coord, rl_coord], [fb_face, rl_face, ud_face])
    else
      return Corner.from_arrs([ud_coord, rl_coord, fb_coord], [rl_face, fb_face, ud_face])
    end
  end

  def valid_orientations
    [self, rotated_clockwise, rotated_clockwise.rotated_clockwise]
  end

  def something
    Set[
      Set[0, 1, 2],
      Set[1, 3, 5],
      Set[0, 4, 5],
      Set[2, 3, 4]
    ]
  end

  def ud_coord
    @data.keys.find(&:u_or_d?)
  end

  def ud_face
    @data[ud_coord]
  end

  def rl_coord
    @data.keys.find(&:r_or_l?)
  end

  def rl_face
    @data[rl_coord]
  end

  def fb_coord
    @data.keys.find(&:f_or_b?)
  end

  def fb_face
    @data[fb_coord]
  end

  def clockwise_twists_to_orient
    if correctly_oriented?
      0
    elsif rotated_clockwise.correctly_oriented?
      1
    else
      2
    end
  end

  def orientation_profile_int
    clockwise_twists_to_orient
  end

  def acube_orientation_string
    {
      0 => "?",
      1 => "-?",
      2 => "+?",
    }[clockwise_twists_to_orient]
  end

  def correctly_oriented?
    ud_face.u_or_d?
  end
end
