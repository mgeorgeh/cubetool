# requires self.solved_state and self#applying_move(move)
module CubeSection
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def sticker_lookup(c)
      {
        'R' => 0,
        'U' => 1,
        'F' => 2,
        'L' => 3,
        'D' => 4,
        'B' => 5
      }[c]
    end

    # doesn't work on something with pieces_to_orient
    def solved_state
      hsh = {}
      all_pieces.each do |str|
        hsh[str] = Piece.solved(str)
      end
      new(hsh)
    end

    def from_scramble(scramble)
      by_applying(scramble, to: solved_state)
    end
   
    def random
      from_scramble(Scramble.new)
    end
   
    def by_applying(moves, to:)
      initial_state = to
      if moves.is_a? String
        moves = MoveSequence.from_string(moves)
      end
      moves.reduce(initial_state) do |state, move|
        state.applying_move(move)
      end
    end
  end

  def initialize(pieces)
    @pieces = pieces
  end

  def ==(other)
    @pieces == other.pieces
  end

  def eql?(other)
    @pieces == other.pieces
  end

  def hash
    @pieces.hash
  end

  # find a particular piece
  def piece(path)
    entry = @pieces.find { |_, p| p.faces == Set.new(path.chars.map {|c| self.class.sticker_lookup(c) })}
    (entry || [nil, nil])[1]
  end

  # find the piece at a particular location
  def piece_at(path)
    entry = @pieces.find { |_, p| p.coords == Set.new(path.chars.map { |c| self.class.sticker_lookup(c) })}
    (entry || [nil, nil])[1]
  end

  def applying_move(move)
    self.class.new(@pieces.transform_values { |p| p.applying_move(move) })
  end
end