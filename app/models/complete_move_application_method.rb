class CompleteMoveApplicationMethod
  # deps: state_type.pieces_to_solve
  def initialize(state_type)
    @state_type = state_type 
  end

  def build_assignment_statement(num_sockets, idx)
    arr = ["000"] * num_sockets
    arr[idx] = "111"
    shift_idx = (num_sockets - idx - 1) * 3
    suffix = shift_idx == 0 ? "" : (">> " + shift_idx.to_s)
    "\ts#{idx} = (state & 0b#{arr.join}) #{suffix}\n" 
  end

  def string
    num_sockets = (@state_type.pieces_to_solve + @state_type.pieces_to_preserve).join.chars.count
    result = ""
    result << "create_method(:apply_move) do |m, state|\n"
    result << "\th = @move_lookup_hash[m]\n"
    result << "\ttc = @this_coord_hash[m]\n"
    i = 0
    (@state_type.pieces_to_solve + @state_type.pieces_to_preserve).each do |p|
      len = p.chars.count
      len.times do
        result << build_assignment_statement(num_sockets, i)
        i += 1
      end
      vars = (i - len..i - 1).to_a.map { |n| "s#{n}" }
      cond = vars.map { |v| "#{v} == tc" }.join(" || ")
      assignments = vars.map { |v| "\t\t#{v} = h[#{v}]\n" }.join
      result << "\tif #{cond}\n"
      result << assignments
      result << "\tend\n"
    end
   
    vars = (0..i - 1).to_a.map do |n| 
      n == (i - 1) ? "s#{n}" : "(s#{n} << #{(num_sockets - n - 1) * 3})"
    end.join(" | ")
    result << "\t#{vars}\n"
    result << "end"
    result
  end
end