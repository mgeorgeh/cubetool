class StateIntEncoding
  def initialize(state)
    @state = state
  end

  def int
    lookup_thing = {
      0 => "00",
      1 => "01",
      2 => "10",
    }
    os = @state.class.pieces_to_orient.map do |p|
      lookup_thing[@state.piece_at(p).orientation_profile_int]
    end.join

    ss = @state.class.pieces_to_solve.map do |p|
      p.chars.map do |c|
        str_sticker_lookup[
          int_sticker_lookup[@state.piece(p).coord(c)]
        ]
      end
    end.flatten.join

    (os + ss).to_i(2)
  end

  def int_sticker_lookup
    {
      0 => 'R',
      1 => 'U',
      2 => 'F',
      3 => 'L',
      4 => 'D',
      5 => 'B',
    }
  end

  def str_sticker_lookup
    {
      'R' => "000",
      'U' => "001",
      'F' => "010",
      'L' => "011",
      'D' => "100",
      'B' => "101",
    }
  end
end