# requires self.solved_state and self#applying_move(move)
module TestCubeSection
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def sticker_lookup(c)
      {
        'R' => 0,
        'U' => 1,
        'F' => 2,
        'L' => 3,
        'D' => 4,
        'B' => 5
      }[c]
    end

    def solved_state
      hsh = {}
      included_pieces.each do |str|
        hsh[str] = solved_piece(str)
      end
      new(hsh)
    end

    def solved_piece(str)
      arr = str.chars.map { |c| sticker_lookup(c) }
      if str.length == 2
        Edge.from_arrs(arr, arr)
      else
        Corner.from_arrs(arr, arr)
      end
    end

    def from_scramble(scramble)
      by_applying(scramble, to: solved_state)
    end
   
    def random
      from_scramble(Scramble.new)
    end
   
    def by_applying(moves, args)
      initial_state = args[:to]
      if moves.is_a? String
        moves = MoveSequence.from_string(moves)
      end
      moves.reduce(initial_state) do |state, move|
        state.applying_move(move)
      end
    end
  end

  def sticker_lookup(c)
    {
      'R' => 0,
      'U' => 1,
      'F' => 2,
      'L' => 3,
      'D' => 4,
      'B' => 5
    }[c]
  end

  def initialize(pieces)
    @pieces = pieces
  end

  def ==(other)
    @pieces == other.pieces
  end

  def eql?(other)
    @pieces == other.pieces
  end

  def hash
    @pieces.hash
  end

  def piece(path)
    @pieces.find { |p| p.faces == Set.new(path.chars.map { |c| sticker_lookup(c) }) }
  end

  def piece_at(path)
    @pieces.find { |p| p.coords == Set.new(path.chars.map { |c| sticker_lookup(c) }) }
    # @pieces.find { |p| p.coords == path.chars.map { |c| sticker_lookup(c) } }
  end

  def applying_move(move)
    # if move.is_a? String
    #   move = Move.from_string(move)
    # end
    self.class.new(@pieces.transform_values { |p| p.applying_move(move) })
    # self.class.new(Set.new(@pieces.map { |p| p.applying_move(move) }))
  end

  # def applying_moves(moves)
  #   self.class.by_applying(moves, to: self)
  # end
end