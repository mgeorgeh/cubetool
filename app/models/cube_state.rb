class CubeState
  include CubeSection
  include Validation
  # attr_accessor :corners, :edges

  @@interface = {
    'R' => {
      'C' => {
        'C' => [0, 1, 1]
      },
      'U' => {
        'F' => [0, 0, 0],
        'M' => [0, 0, 1],
        'B' => [0, 0, 2]
      },
      'F' => {
        'U' => [0, 0, 0],
        'M' => [0, 1, 0],
        'D' => [0, 2, 0]
      },
      'D' => {
        'F' => [0, 2, 0],
        'M' => [0, 2, 1],
        'B' => [0, 2, 2]
      },
      'B' => {
        'U' => [0, 0, 2],
        'M' => [0, 1, 2],
        'D' => [0, 2, 2]
      }
    },
    'U' => {
      'C' => {
        'C' => [1, 1, 1]
      },
      'F' => {
        'L' => [1, 0, 0],
        'M' => [1, 1, 0],
        'R' => [1, 2, 0]
      },
      'B' => {
        'L' => [1, 0, 2],
        'M' => [1, 1, 2],
        'R' => [1, 2, 2]
      },
      'L' => {
        'F' => [1, 0, 0],
        'M' => [1, 0, 1],
        'B' => [1, 0, 2]
      },
      'R' => {
        'F' => [1, 2, 0],
        'M' => [1, 2, 1],
        'B' => [1, 2, 2]
      }
    },
    'F' => {
      'C' => {
        'C' => [2, 1, 1]
      },
      'U' => {
        'L' => [2, 0, 0],
        'M' => [2, 0, 1],
        'R' => [2, 0, 2]
      },
      'D' => {
        'L' => [2, 2, 0],
        'M' => [2, 2, 1],
        'R' => [2, 2, 2]
      },
      'L' => {
        'U' => [2, 0, 0],
        'M' => [2, 1, 0],
        'D' => [2, 2, 0]
      },
      'R' => {
        'U' => [2, 0, 2],
        'M' => [2, 1, 2],
        'D' => [2, 2, 2]
      }
    },
    'L' => {
      'C' => {'C' => [3, 1, 1]},
      'U' => {
        'F' => [3, 0, 0],
        'M' => [3, 0, 1],
        'B' => [3, 0, 2]
      },
      'D' => {
        'F' => [3, 2, 0],
        'M' => [3, 2, 1],
        'B' => [3, 2, 2]
      },
      'F' => {
        'U' => [3, 0, 0],
        'M' => [3, 1, 0],
        'D' => [3, 2, 0]
      },
      'B' => {
        'U' => [3, 0, 2],
        'M' => [3, 1, 2],
        'D' => [3, 2, 2]
      }
    },
    'D' => {
      'C' => { 'C' => [4, 1, 1] },
      'F' => {
        'L' => [4, 0, 0],
        'M' => [4, 1, 0],
        'R' => [4, 2, 0]
      },
      'B' => {
        'L' => [4, 0, 2],
        'M' => [4, 1, 2],
        'R' => [4, 2, 2]
      },
      'L' => {
        'F' => [4, 0, 0],
        'M' => [4, 0, 1],
        'B' => [4, 0, 2]
      },
      'R' => {
        'F' => [4, 2, 0],
        'M' => [4, 2, 1],
        'B' => [4, 2, 2]
      }
    },
    'B' => {
      'C' => { 'C' => [5, 1, 1] },
      'U' => {
        'L' => [5, 0, 0],
        'M' => [5, 0, 1],
        'R' => [5, 0, 2]
      },
      'D' => {
        'L' => [5, 2, 0],
        'M' => [5, 2, 1],
        'R' => [5, 2, 2]
      },
      'L' => {
        'U' => [5, 0, 0],
        'M' => [5, 1, 0],
        'D' => [5, 2, 0]
      },
      'R' => {
        'U' => [5, 0, 2],
        'M' => [5, 1, 2],
        'D' => [5, 2, 2]
      }
    }
  }


  # pub prerequisites for CubeSection
  def self.solved_state
    CubeState.from_array([0, 1, 2, 3, 4, 5].flat_map { |i| [i] * 9 })
  end

  # not pub
  def self.at(path, data)
    ref = @@interface
    path.each_char do |chr|
      ref = ref[chr]
    end
    data[ref[0]][ref[1]][ref[2]]
  end

  # pub
  def self.from_array(arr)
    data = arr.flatten.each_slice(9).map { |s| s.each_slice(3).to_a }

    @types_and_values_correct = arr.is_a?(Array) && 
      arr.flatten.length == 54 && 
      arr.flatten.all? { |i| i.is_a?(Integer) && i.between?(0, 5) }

    result = {}
    corners_arr = [
      ['RUF', 'URF', 'FUR'], ['RUB', 'URB', 'BUR'],
      ['RDF', 'FRD', 'DRF'], ['RDB', 'DBR', 'BRD'],
      ['LUF', 'UFL', 'FLU'], ['LUB', 'ULB', 'BUL'],
      ['LDF', 'FDL', 'DFL'], ['LDB', 'BDL', 'DLB'],
    ]

    edges_arr = [
      ['RUM', 'URM'], ['RFM', 'FRM'], ['RBM', 'BRM'], 
      ['RDM', 'DRM'], ['UFM', 'FUM'], ['UBM', 'BUM'], 
      ['LUM', 'ULM'], ['LFM', 'FLM'], ['LBM', 'BLM'], 
      ['LDM', 'DLM'], ['FDM', 'DFM'], ['BDM', 'DBM'],
    ]

    corners_arr.each do |coords|
      corner = Corner.from_arrs(
        coords.map { |c| sticker_lookup(c[0]) },
        coords.map { |c| at(c, data) }
      )
      result[corner.lookup_string] = corner
    end

    edges_arr.each do |coords|
      edge = Edge.from_arrs(
        coords.map { |c| sticker_lookup(c[0]) },
        coords.map { |c| at(c, data) }
      )
      result[edge.lookup_string] = edge
    end
    self.new(result)
  end

  # may need to add Center support
  def to_array
    [
      piece_at('RUF').face('R'), piece_at('RU').face('R'), piece_at('RUB').face('R'),
      piece_at('RF').face('R'), 0, piece_at('RB').face('R'),
      piece_at('RFD').face('R'), piece_at('RD').face('R'), piece_at('RDB').face('R'),

      piece_at('ULF').face('U'), piece_at('UL').face('U'), piece_at('ULB').face('U'),
      piece_at('UF').face('U'), 1, piece_at('UB').face('U'),
      piece_at('RUF').face('U'), piece_at('UR').face('U'), piece_at('URB').face('U'),

      piece_at('FUL').face('F'), piece_at('UF').face('F'), piece_at('FUR').face('F'),
      piece_at('FL').face('F'), 2, piece_at('FR').face('F'),
      piece_at('FDL').face('F'), piece_at('FD').face('F'), piece_at('FDR').face('F'),

      piece_at('LUF').face('L'), piece_at('LU').face('L'), piece_at('LUB').face('L'),
      piece_at('LF').face('L'), 3, piece_at('LB').face('L'),
      piece_at('LDF').face('L'), piece_at('LD').face('L'), piece_at('LDB').face('L'),

      piece_at('DFL').face('D'), piece_at('DL').face('D'), piece_at('DLB').face('D'),
      piece_at('DF').face('D'), 4, piece_at('DB').face('D'),
      piece_at('DFR').face('D'), piece_at('DR').face('D'), piece_at('DRB').face('D'),

      piece_at('BUL').face('B'), piece_at('BU').face('B'), piece_at('BUR').face('B'),
      piece_at('BL').face('B'), 5, piece_at('BR').face('B'),
      piece_at('BLD').face('B'), piece_at('BD').face('B'), piece_at('BRD').face('B')
    ]    
  end
end
