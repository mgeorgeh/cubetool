module MethodStep

  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def varying_transforms
      [TransformationSequence.from_move_list([NullRotation])]
    end

    def solution_type
      (self.to_s[0..-6] + "Solution").constantize
    end

    def solution_table_name
      solution_type.table_name
    end

    def pieces_to_preserve
      []
    end

    def pieces_to_orient
      []
    end

    def pieces_to_preserve_orientation
      []
    end

    def pieces_to_solve
      []
    end

    def all_pieces
      (pieces_to_solve + 
        pieces_to_orient +
        pieces_to_preserve +
        pieces_to_preserve_orientation).uniq
      # [
      #   :pieces_to_solve, 
      #   :pieces_to_orient,
      #   :pieces_to_preserve,
      #   :pieces_to_preserve_orientation
      # ].reduce([]) { |acc, m| self.send(m) + acc }.uniq
    end

  end

  def reachable?
    true
  end

  def solutions
    i = StateIntEncoding.new(self).int
    self.class
      .solution_type
      .where(state: i.to_s)
      .map { |s| MoveSequence.from_string(s.solution) }
  end
end

