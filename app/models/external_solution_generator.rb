class ExternalSolutionGenerator
  def initialize(state_type)
    @state_type = state_type
  end

  def run
    # sts = @state_type.possible_states
    sts = PossibleStatesGenerator.new(@state_type).states
    results = {}
    i = 0
    sts.each do |st|
      p i if i % 100 == 0
      int = StateIntEncoding.new(st).int
      results[int] = generate_solutions(st)
      i += 1
    end
    vals = ""
    results.each do |state, sols|
      sols.each do |sol|
        vals << "('#{state}', '#{sol.tr("'", "i")}'), "
      end
    end

    vals.chop!
    vals.chop!
    solution_type = @state_type.solution_type
    solution_table_name = @state_type.solution_table_name
    solution_type.transaction do        
      cols = solution_type.column_names.drop(1).join(", ")
      solution_type.connection.exec_query "INSERT INTO #{solution_table_name} (#{cols}) VALUES #{vals}"
    end
  end

  def generate_solutions(st)
    f = File.open('test_thing.txt', 'w')
    f.truncate(0)
    f << AcubeInputBuilder.new(st).acube_input
    f.close
    str = `java -jar acube.jar test_thing.txt`
    xs = str.split("\n")
       .select { |l| l.length > 0 && l.last == ")" && l[0] != "|" }
       .map { |s| s.chars.take_while { |c| c != '(' }.join.tr(" ", "") }
    xs
  end
end
