class Scramble
  include Enumerable

  attr_accessor :move_string, :sequence

  def initialize(len = 25)
    move_list = [random_base + random_modifier]
    prev = move_list[0]
    (len - 1).times do
      next_move = random_move_string(prev)
      move_list.push(next_move)
      prev = next_move
    end
    @move_string = move_list.join
    @sequence = MoveSequence.from_string(@move_string)
  end

  def each
    @sequence.each do |m|
      yield m
    end
  end

  def to_s
    @sequence.to_s
  end

  def complements
    {
      'B' => 'F',
      'F' => 'B',
      'D' => 'U',
      'U' => 'D',
      'L' => 'R',
      'R' => 'L'
    }
  end

  def rng
    @rng ||= Random.new
  end

  def base_moves
    ['B', 'D', 'F', 'L', 'R', 'U']
  end

  def random_base
    base_moves[rng.rand(0..5)]
  end

  def modifiers
    ['', '2', "'"]
  end

  def random_modifier
    modifiers[rng.rand(0..2)]
  end

  def random_move_string(prev)
    (base_moves - [prev[0], complements[prev[0]]])[rng.rand(0..3)] + random_modifier
  end
end
