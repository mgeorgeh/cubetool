class MultiBaseCounter
  attr_accessor :values
  def initialize(bases)
    @bases = bases
    @values = Array.new(bases.length) { |_| 0 }
  end

  def add(n)
    add_sub(n, 0)
  end

  def inc
    add(1)
  end

  def add_sub(n, pos)
    # puts "add_sub(#{n}, #{pos})"
    v = @values[pos] + n
    # puts "vp: #{@values[pos]}"
    # puts "v: #{v}"
    @values[pos] = v % @bases[pos]
    # puts "vals: #{@values}"
    next_n = v / @bases[pos]
    # puts "next_n: #{next_n}"
    if next_n != 0
      # puts "np: #{(pos + 1) % @bases.length}"
      np = (pos + 1)
      if np == @bases.length
        return
      else
        add_sub(next_n, np) 
      end
    end
  end

  def zero?
    values.all?(&:zero?)
  end
end