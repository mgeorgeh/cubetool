class Edge < Piece
  def self.valid_coords
    Set[
      Set[0, 1], Set[0, 4], Set[0, 2], Set[0, 5], Set[1, 2], Set[1, 5],
      Set[4, 2], Set[4, 5], Set[3, 1], Set[3, 4], Set[3, 2], Set[3, 5]
    ]
  end

  def flips_to_orient
    correctly_oriented? ? 0 : 1
  end

  def orientation_profile_int
    flips_to_orient
  end

  def acube_orientation_string
    {
      0 => "?",
      1 => "-?",
    }[flips_to_orient]
  end

  def flipped
    c_arr = coords.to_a
    f_arr = c_arr.map { |c| face(c) }
    Edge.from_arrs(c_arr.reverse, f_arr) 
  end

  def valid_orientations
    [self, flipped]
  end

  def correctly_oriented?
    if in_top_or_bottom?
      color_on_ud_face.send(belongs_in_top_or_bottom? ? :u_or_d? : :f_or_b?)
    elsif belongs_in_top_or_bottom?
      !color_on_rl_face.u_or_d?
    else
      color_on_rl_face.r_or_l?
    end
  end

  def in_top_or_bottom?
    @data.keys.any?(&:u_or_d?)
  end

  def belongs_in_top_or_bottom?
    @data.values.any?(&:u_or_d?)
  end

  def color_on_ud_face
    @data[@data.keys.find(&:u_or_d?)]
  end

  def color_on_rl_face
    @data[@data.keys.find(&:r_or_l?)]
  end
end
