# wrapper around an array of hashes
# each hash represents a previous action taken on a cube state
# ie ["solved the Cross on the D face", "solved Corner-edge pair 1 on RF", etc]
class MethodPath
  include Enumerable

  def self.empty
    new([])
  end

  def each
    data.each { |x| yield x }
  end

  def initialize(raw)
    @raw = raw
    # current form: [{transform: "T", step_type: "ClassName"}]
  end

  def data
    @data ||= @raw.map do |item|
      {
        transform: item[:transform].map(&:constantize),
        step_type: item[:step_type].constantize,
      }
    end
  end

  def valid?
    true
    # @strs.all? { |s| Object.const_defined?(s) }
    # and all are steps to the same method
  end

  def empty?
    count == 0
  end

  def last
    data.last
  end

  def transforms
    TransformationSequence.from_move_list(data.flat_map { |item| item[:transform] })
  end
end