# require 'ruby-prof'

# require 'json'
# require 'benchmark'

# def measure(&block)
#   no_gc = (ARGV[0] == '--no-gc')

#   if no_gc
#     GC.disable
#   else
#     GC.start
#   end

#   memory_before = `ps -o rss= -p #{Process.pid}`.to_i / 1024
#   gc_stat_before = GC.stat
#   time = Benchmark.realtime do
#     yield
#   end
#   puts ObjectSpace.count_objects
#   unless no_gc
#     GC.start(full_mark: true, immediate_sweep: true, immediate_mark: false)
#   end
#   puts ObjectSpace.count_objects
#   gc_stat_after = GC.stat
#   memory_after = `ps -o rss= -p #{Process.pid}`.to_i / 1024
#   puts({
#     RUBY_VERSION => {
#       gc: no_gc ? 'disabled' : 'enabled',
#       time: time.round(2),
#       gc_count: gc_stat_after[:count] - gc_stat_before[:count],
#       memory: "%d MB" % (memory_after - memory_before)
#     }
#   }.to_json)
# end
# # profile the code
# # RubyProf.start

# # GC.disable
# # measure do
#   params = {
#     step: 1,
#     method: 'Fridrich',
#     move_set: 'ALL',
#     turn_metric: 'HTM',
#     solution: '',
#     cube_state: CubeState.from_scramble("BDR2U2F'R2F2LR'D2L'RF'U'RFDBL'U2D'FR2U'D2").arr
#   }
#   s = Solver.new(params)
#   s.solve
# # end

# # puts time.round(2)
# # result = RubyProf.stop

# # print a flat profile to text
# # printer = RubyProf::FlatPrinter.new(result)
# # printer.print(STDOUT)
