class Solver
  def initialize(params)
    @params = params
  end

  # :solution,
  # :cube_state,
  # :move_set,
  # :turn_metric,
  # :method,
  # :step => :path (["ClassName"])
  # :next_cube_state => initial condition
  # :move_set + :turn_metric => set of things that count as 1 move
  # :method + :step => condition to be satisfied

  # because of lazy loading, using Object.const_defined?("ClassName") could
  # fail erroneously. using constantize and rescuing errors is more robust

  def solve
    {
      cube_state: next_cube_state.to_array,
      solutions: cube_method.solutions_for(path: path, state: next_cube_state)
    }
  end

  def method_step_solved?(cube_state)
    cube_method.step_solved?(path, cube_state)
  end

  # Related To Validation and not solving
  def valid?
    keys_present? &&
      fields_valid? &&
      valid_method_step? &&
      current_step_solved_by_solution? &&
      next_step_solvable_using_moveset?
  end

  # private methods:

  # implement
  # Related To Validation and not solving
  def current_step_solved_by_solution?
    true
  end

  def next_cube_state
    @next_cube_state ||= CubeState.by_applying(solution, to: cube_state)
  end

  # implement
  # Related To Validation and not solving
  def next_step_solvable_using_moveset?
    true
  end

  # Related To Validation and not solving
  def keys_present?
    [
      :solution,
      :cube_state,
      :move_set,
      :turn_metric,
      :method,
      :path
    ].all? { |k| @params.key?(k) }
  end

  # Related To Validation and not solving
  def fields_valid?
    [solution, cube_state, move_set, turn_metric, method, path].all?(&:valid?)
  end

  # temp
  def valid_method_step?
    true
  end

  def path
    @path ||= MethodPath.new(@params[:path])
  end

  def solution
    @solution ||= MoveSequence.from_string(@params[:solution])
  end

  def cube_state
    @cube_state ||= CubeState.from_array(@params[:cube_state])
  end

  # kaput
  def move_set
    @move_set ||= MoveSet.new(@params[:move_set])
  end

  # kaput
  def turn_metric
    @turn_metric ||= TurnMetric.new(@params[:turn_metric])
  end

  def cube_method
    @cube_method ||= CubeMethod.from_string(@params[:method])
  end
end
