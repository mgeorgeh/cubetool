class TurnMetric
  def initialize(str)
    @str = str
  end

  def valid?
    metric_hash.keys.include? @str
  end

  def count_turns(move)
    metric_hash[@str].call(move)
  end

  # private methods:

  def metric_hash
    {
      'QTM' => lambda do |m|
        return 2 if [R2, U2, F2, L2, D2, B2].include?(m)
        return 1
      end,
      'HTM' => lambda do |_|
        return 1
      end
    }
  end
end
