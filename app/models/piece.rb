class Piece
  attr_accessor :data

  @@sticker_lookup = {
    'R' => 0,
    'U' => 1,
    'F' => 2,
    'L' => 3,
    'D' => 4,
    'B' => 5
  }

  def self.solved(str)
    arr = str.chars.map { |c| @@sticker_lookup[c] }
    if str.length == 1
      Center.from_arrs(arr, arr)
    elsif str.length == 2
      Edge.from_arrs(arr, arr)
    elsif str.length == 3
      Corner.from_arrs(arr, arr)
    end
  end

  def valid_instances
    self.class.valid_coords.flat_map { |cs| valid_instances_at(cs) }
  end

  def lookup_string
    @data.values.map { |i| @@sticker_lookup.key(i) }.join
  end

  def valid_instances_at(dest_coords)
    moves = Move.all

    frontier = [self]
    result = nil
    loop do
      item = frontier.shift
      if item.coords == dest_coords
        result = item
        break
      end
      moves.each do |m|
        frontier << item.applying_move(m)
      end
    end

    result.valid_orientations
  end

  def self.from_arrs(coords, faces)
    p = new
    d = {}
    i = 0
    while i < coords.length 
      d[coords[i]] = faces[i]
      i += 1
    end
    p.data = d
    p
  end

  def self.from_data(data)
    p = self.new
    p.data = data
    p
  end

  def face(coord)
    if coord.is_a? String
      coord = @@sticker_lookup[coord]
    end
    @data[coord]
  end

  def coord(face)
    if face.is_a? String
      face = @@sticker_lookup[face]
    end
    @data.key(face)
  end

  def coords
    Set.new(@data.keys)
  end

  def faces
    Set.new(@data.values)
  end

  def solved?
    @data.all? {|k, v| k == v}
  end

  def permuted_correctly?
    coords == faces
  end

  def ==(other)
    @data == other.data
  end

  def eql?(other)
    @data == other.data
  end

  def hash
    @data.hash
  end

  def self.valid_coords
    raise NotImplementedError
  end

  def applying_move(move)
    if move.is_a? String
      move = Move.from_string(move)
    end
    self.class.from_data(move.transform(@data))
  end

  def applying_moves(moves)
    if moves.is_a? String
      moves = MoveSequence.from_string(moves)
    end
    moves.reduce(self) do |state, move|
      state.applying_move(move)
    end
  end
end
