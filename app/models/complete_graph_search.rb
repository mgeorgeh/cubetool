# frozen_string_literal: true
# traverses the entire solution space for a state type and saves the results to the database
class CompleteGraphSearch
  def initialize(state_type)
    @state_type = state_type
    @solution_type = state_type.solution_type
    @solution_table_name = state_type.solution_table_name
    @prev = Set.new
    # @solved_state = state_type.solved_state
    # @solved_state_int = @state_type.solved_hack_state_int
    @solved_state_int = StateIntEncoding.new(@state_type.solved_state).int
    @current = Set.new([@solved_state_int])
    @nexts = Set.new
    @result = {}
    @moves = [
      "R", "Ri", "R2", 
      "U", "Ui", "U2", 
      "F", "Fi", "F2", 
      "L", "Li", "L2", 
      "D", "Di", "D2", 
      "B", "Bi", "B2",
    ]
    @move_lookup_hash = {
      "R"  => R.cycle_hash,
      "Ri" => Ri.cycle_hash,
      "R2" => R2.cycle_hash,
      "U"  => U.cycle_hash,
      "Ui" => Ui.cycle_hash,
      "U2" => U2.cycle_hash,
      "F"  => F.cycle_hash,
      "Fi" => Fi.cycle_hash,
      "F2" => F2.cycle_hash,
      "L"  => L.cycle_hash,
      "Li" => Li.cycle_hash,
      "L2" => L2.cycle_hash,
      "D"  => D.cycle_hash,
      "Di" => Di.cycle_hash,
      "D2" => D2.cycle_hash,
      "B"  => B.cycle_hash,
      "Bi" => Bi.cycle_hash,
      "B2" => B2.cycle_hash,
    } 

    @this_coord_hash = {
      "R"  => 0,
      "Ri" => 0,
      "R2" => 0,
      "U"  => 1,
      "Ui" => 1,
      "U2" => 1,
      "F"  => 2,
      "Fi" => 2,
      "F2" => 2,
      "L"  => 3,
      "Li" => 3,
      "L2" => 3,
      "D"  => 4,
      "Di" => 4,
      "D2" => 4,
      "B"  => 5,
      "Bi" => 5,
      "B2" => 5,
    }

    @inverse_hash = {
      "R"  => R.inverse.to_s,
      "Ri" => Ri.inverse.to_s,
      "R2" => R2.inverse.to_s,
      "U"  => U.inverse.to_s,
      "Ui" => Ui.inverse.to_s,
      "U2" => U2.inverse.to_s,
      "F"  => F.inverse.to_s,
      "Fi" => Fi.inverse.to_s,
      "F2" => F2.inverse.to_s,
      "L"  => L.inverse.to_s,
      "Li" => Li.inverse.to_s,
      "L2" => L2.inverse.to_s,
      "D"  => D.inverse.to_s,
      "Di" => Di.inverse.to_s,
      "D2" => D2.inverse.to_s,
      "B"  => B.inverse.to_s,
      "Bi" => Bi.inverse.to_s,
      "B2" => B2.inverse.to_s,
    } 
    method_string = MoveApplicationMethod.new(@state_type).string
    instance_eval(method_string)
  end

  def create_method(name, &block)
    self.class.send(:define_method, name, &block)
  end

  def run
    depth = 0
    loop do
      puts "depth: #{depth}, prev: #{@prev.count}, curr: #{@current.count}, nexts: #{@nexts.count}, res: #{@result.count}"
      @current.each do |state|
        @moves.each do |m|
          next_state = apply_move(m, state)

          if @current.include?(next_state) || @prev.include?(next_state)
            next
          else
            @nexts.add(next_state)
            (@result[next_state] || @result[next_state] = []) << @inverse_hash[m]
          end
        end
      end
      if @nexts.empty?
        break
      end
      @prev = @current
      @current = @nexts
      @nexts = Set.new
      depth += 1
    end
    puts "gen done. found #{@result.count} items"

    vals = "".dup
    i = 0
    @result.each do |state, _|
      s = solutions(state)
      s.each do |sol|
        vals << "('#{state}', '#{sol}'), "
      end

      if i == 1000
        vals.chop!
        vals.chop!
        do_database_stuff(vals)
        vals = "".dup
        i = 0
      end
      i += 1
    end

    vals.chop!
    vals.chop!
    do_database_stuff(vals)

    puts "%d MB" % (`ps -o rss= -p #{Process.pid}`.to_i/1024)
  end

  def solutions(state_int)
    result = []
    frontier = [[state_int, ""]]
    while frontier.length > 0
      state, sol = frontier.shift
      if state == @solved_state_int
        result << sol
        next
      end
      next_moves = @result[state]
      next_moves.each do |m|
        frontier << [apply_move(m, state), sol + m]
      end
    end
    result
  end

  def do_database_stuff(vals)
    sleep(0.002)
    # @solution_type.transaction do
    #   cols = @solution_type.column_names.drop(1).join(", ")
    #   @solution_type.connection.exec_query "INSERT INTO #{@solution_table_name} (#{cols}) VALUES #{vals}" 
    # end
  end
end