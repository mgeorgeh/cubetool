# Takes in a Cube State and returns a string representing that state
# that can be fed into acube
# UF UR UB UL DF DR DB DL FR FL BR BL UFR URB UBL ULF DRF DFL DLB DBR
# @? @? @? @? @? DR @? DL FR FL BR BL ULF BLU RUF URB DRF DFL DLB DBR
# coordinates, not faces. 
# replace templates with faces that are at those coordinates 

# @: ignore orientation, preserve position
# ?: ignore position, preserve orientation
# @?: ignore this piece
# -: (edge): flip piece, (corner): turn counter-clockwise
# +: (corner): turn clockwise
# corner twists may be backwards


# @UR: keep position constant, flip or not
# -?: flip the piece, 
class AcubeInputBuilder
  def initialize(state)
    @state = state
  end

  def canonical_piece_ordering
    [
      'UF', 'UR', 'UB', 'UL', 
      'DF', 'DR', 'DB', 'DL',
      'FR', 'FL', 'BR', 'BL',
      'UFR', 'URB', 'UBL', 'ULF',
      'DRF', 'DFL', 'DLB', 'DBR',
    ] 
  end

  def canonicalize(piece_str)
    canonical_piece_ordering.find do |c_str|
      Set.new(c_str.chars) == Set.new(piece_str.chars)
    end
  end

  def pieces_to_preserve
    @pieces_to_preserve ||= (
      @state.class.pieces_to_preserve.map { |s| canonicalize(s) }
    )
  end

  def pieces_to_orient
    @pieces_to_orient ||= (
      @state.class.pieces_to_orient.map { |s| canonicalize(s) }
    )
  end

  def pieces_to_solve
    @pieces_to_solve ||= (
      @state.class.pieces_to_solve.map { |s| canonicalize(s) }
    )
  end

  def pieces_to_preserve_orientation
    @pieces_to_preserve_orientation ||= (
      @state.class.pieces_to_preserve_orientation.map { |s| canonicalize(s) }
    )
  end

  def all_pieces
    (pieces_to_solve +
      pieces_to_orient +
      pieces_to_preserve +
      pieces_to_preserve_orientation).uniq
  end

  def int_to_char_sticker_lookup(c)
    {
      0 => 'R',
      1 => 'U',
      2 => 'F',
      3 => 'L',
      4 => 'D',
      5 => 'B',
    }[c]
  end

  def acube_input
    piece_strs = []
    canonical_piece_ordering.each do |str|

      p = @state.piece_at(str)

      if !p
        # das ist ein StupidHack
        if str.length == 2 && pieces_to_preserve_orientation.include?("UF")
          piece_strs << "?"
        else
          piece_strs << "@?"
        end
        next
      end

      p_str = canonicalize(p.lookup_string)

      if pieces_to_preserve.include?(p_str)
        piece_strs << p_str
      elsif pieces_to_orient.include?(p_str)
        piece_strs << p.acube_orientation_string
      else #pieces_to_solve.include?(p_str)
        piece_strs << str.chars.map { |c| int_to_char_sticker_lookup(p.face(c)) }.join
      end
    end
    pieces_str = piece_strs.join(" ")
    rest = "\n3: R* U* F* L* D* B*\n4: ftm\n5: 20\n6: yes\n7: yes\n\n"
    "1: " + pieces_str + rest
  end
end