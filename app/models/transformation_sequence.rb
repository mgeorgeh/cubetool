class TransformationSequence
  include Enumerable

  attr_accessor :moves

  def self.from_move_list(list)
    seq = self.new
    seq.moves = list
    seq
  end

  def self.empty
    self.from_move_list([])
  end

  def appending(t)
    self.class.from_move_list(moves + [move])
  end

  def concatenating(ts)
    self.class.from_move_list(moves + ts.moves)
  end

  def inverse
    self.class.from_move_list(moves.map(&:inverse).reverse)
  end

  def transformed(t)
    self.class.from_move_list(moves.map {|m| t.change_move(m)})
  end

  def each
    moves.each { |m| yield m }
  end

  def to_json_output
    map(&:to_s)
  end

  def apply_to(seq)
    reduce(seq) do |sequence, t|
      sequence.transformed(t) 
    end
  end
end