// I : 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53
// X : 2  5  8  1  4  7  0  3  6  45 48 51 46 49 52 47 50 53 11 14 17 10 13 16 9  12 15 29 32 35 28 31 34 27 30 33 18 21 24 19 22 25 20 23 26 38 41 44 37 40 43 36 39 42
// Y : 18 19 20 21 22 23 24 25 26 11 14 17 10 13 16 9  12 15 29 28 27 32 31 30 35 34 33 45 46 47 48 49 50 51 52 53 38 41 44 37 40 43 36 39 42 2  1  0  5  4  3  8  7  6
// Z : 42 43 44 39 40 41 36 37 38 0  1  2  3  4  5  6  7  8  20 23 26 19 22 25 18 21 24 15 16 17 12 13 14 9  10 11 27 28 29 30 31 32 33 34 35 47 50 53 46 49 52 45 48 51 

export function composeCubeTransform(a,b) {
  let result = [];
  a.forEach((j,i) => {
    result[i] = b[j];
  });
  return result;
}

export function applyCubeTransform(transform, state) {
  let result = [];
  transform.forEach((j, i) => {
    result[j] = state[i]
  });
  return result;
}

export function composeSolutionTransform(a,b) {
  let result = {};
  ["R", "U", "F", "L", "D", "B"].forEach((move) => {
    result[move] = b[a[move]];
  });
  return result;
}

let moveChars = {
  "R" : true,
  "U" : true,
  "F" : true,
  "L" : true,
  "D" : true,
  "B" : true,
};

// transform :: {String => String}
export function applySolutionTransform(transform, solution) {
  let result = "";
  for (let char of solution) {
    if (moveChars[char]) {
      char = transform[char];
    }
    result += char;
  }
  return result;
}

export function inverseSolutionTransform(transform) {
  let result = {};
  for (let key in transform) {
    result[transform[key]] = key;
  }
  return result;
}

export let cubeTransforms = {
  "i" : [
    0,  1,  2,  3,  4,  5,  6,  7,  8,  
    9,  10, 11, 12, 13, 14, 15, 16, 17, 
    18, 19, 20, 21, 22, 23, 24, 25, 26, 
    27, 28, 29, 30, 31, 32, 33, 34, 35, 
    36, 37, 38, 39, 40, 41, 42, 43, 44, 
    45, 46, 47, 48, 49, 50, 51, 52, 53,
  ],
  "x" : [
    2,  5,  8,  1,  4,  7,  0,  3,  6,
    45, 48, 51, 46, 49, 52, 47, 50, 53,
    11, 14, 17, 10, 13, 16, 9,  12, 15,
    29, 32, 35, 28, 31, 34, 27, 30, 33, 
    18, 21, 24, 19, 22, 25, 20, 23, 26, 
    38, 41, 44, 37, 40, 43, 36, 39, 42,
  ],
  "y" : [
    18, 19, 20, 21, 22, 23, 24, 25, 26, 
    11, 14, 17, 10, 13, 16, 9,  12, 15, 
    29, 28, 27, 32, 31, 30, 35, 34, 33, 
    45, 46, 47, 48, 49, 50, 51, 52, 53, 
    38, 41, 44, 37, 40, 43, 36, 39, 42, 
    2,  1,  0,  5,  4,  3,  8,  7,  6,
  ],
  "z" : [
    42, 43, 44, 39, 40, 41, 36, 37, 38, 
    0,  1,  2,  3,  4,  5,  6,  7,  8,  
    20, 23, 26, 19, 22, 25, 18, 21, 24, 
    15, 16, 17, 12, 13, 14, 9,  10, 11, 
    27, 28, 29, 30, 31, 32, 33, 34, 35, 
    47, 50, 53, 46, 49, 52, 45, 48, 51,
  ], 
};
export let solutionTransforms = {
  "i" : {
    "R" : "R",
    "U" : "U",
    "F" : "F",
    "L" : "L",
    "D" : "D",
    "B" : "B",
  },
  "x" : {
    "R" : "R",
    "U" : "B",
    "F" : "U",
    "L" : "L",
    "D" : "F",
    "B" : "D",
  },
  "y" : {
    "R" : "F",
    "U" : "U",
    "F" : "L",
    "L" : "B",
    "D" : "D",
    "B" : "R",
  },
  "z" : {
    "R" : "D",
    "U" : "R",
    "F" : "F",
    "L" : "U",
    "D" : "L",
    "B" : "B",
  },
};
