import { 
  composeCubeTransform, 
  applyCubeTransform,
  composeSolutionTransform,
  applySolutionTransform,
  inverseSolutionTransform,
  cubeTransforms,
  solutionTransforms,
} from "./transform";

import { renderCubeState } from "./drawing";

document.addEventListener("DOMContentLoaded", function(event) {
  let methodNames = Array.from(document.getElementsByTagName("label")).map((elt) => elt.textContent.trim());
  methodNames.push("None");
  let initialScrambles = {};
  let initialFetches = {};
  let initialCubeStates = {};
  let initialSteps = {};
  let initialSolutions = {};
  let initialPaths = {};
  let initialCubeTransforms = {};
  let initialSolutionTransforms = {};
  let initialSolutionVisibility = {};
  let solvedState = [
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 1, 1, 1, 1, 1, 1, 1, 1,
    2, 2, 2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3, 3, 3, 3, 3, 3,
    4, 4, 4, 4, 4, 4, 4, 4, 4,
    5, 5, 5, 5, 5, 5, 5, 5, 5,
  ];

  methodNames.forEach((elt) => {
    initialScrambles[elt] = "";
    initialFetches[elt] = false;
    initialCubeStates[elt] = [solvedState];
    initialSteps[elt] = 0;
    initialSolutions[elt] = [[]];
    initialPaths[elt] = [];
    initialCubeTransforms[elt] = cubeTransforms["i"];
    initialSolutionTransforms[elt] = solutionTransforms["i"];
    initialSolutionVisibility[elt] = [true];
  });

  let app = {
    scrambles : initialScrambles, 
    initialFetchDoneFor : initialFetches,
    cubeStates : initialCubeStates,
    steps : initialSteps,
    solutions : initialSolutions,
    paths : initialPaths,
    cubeTransforms : initialCubeTransforms,
    solutionTransforms : initialSolutionTransforms,
    solutionsVisible : initialSolutionVisibility,
    currentMethod : "None",
    cubeDisplay : document.getElementById("cube-display"),
    scrambleDisplay : document.getElementById("scramble-display"),
    solutionsDisplay : document.getElementById("solutions"),
    colors: {
      0: "rgb(200, 0, 0)",
      1: "rgb(0, 0, 200)",
      2: "rgb(255, 255, 255)",
      3: "rgb(255, 140, 0)",
      4: "rgb(53, 183, 0)",
      5: "rgb(232, 222, 37)"
    },
  };

  function localTransformedCubeState() {
    return applyCubeTransform(currentLocalCubeTransform(), currentCubeState());
  }

  function localTransformedSolutions() {
    let sols = currentSolutions();
    let tr = currentLocalSolutionTransform();
    let result = [];
    sols.forEach((sol) => {
      let r_obj = {};
      r_obj.label = applySolutionTransform(tr,sol.label);
      r_obj.transform = sol.transform;
      r_obj.step_type = sol.step_type;
      r_obj.solution_length = sol.solution_length;
      r_obj.solutions = [];
      sol.solutions.forEach((solStr) => {
        r_obj.solutions.push(applySolutionTransform(tr,solStr));
      });
      result.push(r_obj);
    });
    return result
  }

  function currentCubeState() {
    return app.cubeStates[currentMethod()][currentStep()];
  }

  function currentLocalCubeTransform() {
    return app.cubeTransforms[currentMethod()];
  }

  function currentLocalSolutionTransform() {
    return app.solutionTransforms[currentMethod()];
  }

  function currentSolutions() {
    return app.solutions[currentMethod()][currentStep()];
  }

  function currentStep() {
    return app.steps[currentMethod()];
  }

  function currentPath() {
    return app.paths[currentMethod()];
  }

  function currentSolutionVisibility() {
    return app.solutionsVisible[currentMethod()][currentStep()];
  }

  function currentMethod() {
    return app.currentMethod
  }

  function displaySolutions() {
    let solutionsDisplay = app.solutionsDisplay;
    while (solutionsDisplay.firstChild) {
      solutionsDisplay.removeChild(solutionsDisplay.firstChild);
    }

    let overlay = solutionOverlayNode();
    solutionsDisplay.appendChild(overlay);
    if (currentSolutionVisibility()) {
      overlay.style.display = "none";
    }

    localTransformedSolutions().forEach((solutionSet) => {
      let container = solutionContainerNode(solutionSet);
      solutionsDisplay.appendChild(container);

      solutionSet.solutions.forEach((solution) => {
        let elt = solutionElement(
          solution, 
          solutionSet.transform, 
          solutionSet.step_type, 
          solutionSet.label
        );
        container.appendChild(elt);
        if (currentSolutionVisibility()) {
          container.style.display = "block";
        }
      });
    });
  }

  function solutionOverlayNode() {
    let solutionOverlay = document.createElement('div');
    let content = document.createElement('p');
    content.className = "solution-overlay-content";
    content.appendChild(document.createTextNode("Click to reveal solutions"));
    solutionOverlay.appendChild(content);
    solutionOverlay.className = "solution-overlay";
    solutionOverlay.addEventListener('click', (event) => {
      solutionOverlay.style.display = "none";
      app.solutionsVisible[currentMethod()][currentStep()] = true;
      Array.from(document.getElementsByClassName("solution-container")).forEach((container) => {
        container.style.display = "block";
      });
    });
    return solutionOverlay;
  }

  function solutionContainerNode(solutionSet) {
    let container = document.createElement('div');
    container.appendChild(document.createTextNode(solutionSet.label + " (" + solutionSet.solution_length + ")"));
    container.className = "solution-container";
    container.style.display = "none";
    return container;
  }

  function displayScramble() {
    app.scrambleDisplay.textContent = "scramble : " + 
      applySolutionTransform(currentLocalSolutionTransform(), app.scrambles[currentMethod()]);
  }

  function displayCubeState() {
    renderCubeState(localTransformedCubeState(), app);
  }

  function reDisplay() {
    displayScramble();
    displayCubeState();
    displaySolutions();
  }

  function setSelectedMethod(methodName) {
    app.currentMethod = methodName;
  }

  function solutionElement(solution, transform, step_type, label) {
    if (solution === " ") {
      solution = "Done";
    }
    let tn = document.createTextNode(solution);
    let elt = document.createElement("div");
    elt.appendChild(tn);
    elt.addEventListener('click', (event) => {
      apiFetch(solution, transform, step_type).then(reDisplay);
    })
    return elt;
  }

  function apiFetch(solution, transform, step_type) {
    return new Promise((resolve) => {

      if (arguments.length === 3) {
        app.paths[currentMethod()][currentStep()] = {
          transform : transform,
          step_type : step_type,
        };
        app.paths[currentMethod()].splice(currentStep() + 1, 100);
      }

      if (solution == "Done") {
        solution = " ";
      }

      const jsonBody = {
        "cube_state" : currentCubeState(),
        "solution" : applySolutionTransform(inverseSolutionTransform(currentLocalSolutionTransform()), solution),
        "method" : currentMethod(),
        "path" : currentPath(),
        "move_set" : "ALL",
        "turn_metric" : "htm",
      }

      fetch('/api/step', {
        method : 'POST',
        headers : {'Content-Type' : 'application/json', 'Accept' : 'application/json'},
        body : JSON.stringify(jsonBody),
      }).then((response) => response.json()).then((resultJson) => {
        app.steps[currentMethod()] += 1;
        app.cubeStates[currentMethod()][currentStep()] = resultJson.cube_state;
        app.cubeStates[currentMethod()].splice(currentStep() + 1, 100);
        app.solutionsVisible[currentMethod()][currentStep()] = false;
        app.solutions[currentMethod()][currentStep()] = resultJson.solutions.sort((a,b) => {
          return a.solution_length - b.solution_length;
        });
        app.solutions[currentMethod()].splice(currentStep() + 1, 100);
        if (resultJson.hasOwnProperty("scramble")) {
          app.scrambles[currentMethod()] = resultJson.scramble;
        }
      }).then(resolve);
    });
  }

  Array.from(document.getElementsByClassName("method-selector")).forEach((elt) => {
    elt.addEventListener('click', (event) => {
      let methodName = elt.textContent.trim();
      setSelectedMethod(methodName);
      if (!app.initialFetchDoneFor[currentMethod()]) {
        app.initialFetchDoneFor[currentMethod()] = true;
        apiFetch('').then(reDisplay);
      } else {
        reDisplay();
      }
    })
  });

  ["x","y","z"].forEach((dir) => {
    document.getElementById("rot_" + dir).addEventListener('click', function(event) {
      app.cubeTransforms[currentMethod()] = composeCubeTransform(currentLocalCubeTransform(), cubeTransforms[dir]);
      app.solutionTransforms[currentMethod()] = composeSolutionTransform(currentLocalSolutionTransform(), solutionTransforms[dir]);
      reDisplay();
    });
  });

  document.getElementById("refresh").addEventListener('click', (event) => {
    app.scrambles[currentMethod()] = "";
    app.cubeStates[currentMethod()] = [solvedState];
    app.paths[currentMethod()] = [];
    app.steps[currentMethod()] = 0;
    app.solutions[currentMethod()] = [[]];
    apiFetch('').then(reDisplay);
  });

  document.getElementById("backward").addEventListener('click', (event) => {
    if (currentStep() > 0) {
      app.steps[currentMethod()] -= 1;
      reDisplay();
    }
  });

  document.getElementById("forward").addEventListener("click", (event) => {
    if (app.cubeStates[currentMethod()][currentStep() + 1] != undefined) {
      app.steps[currentMethod()] += 1;
      reDisplay();
    }
  });

  window.addEventListener('resize', (event) => {
    displayCubeState();
  })

  // this is a hack.
  // safari doesn't display canvas properly if redisplay is called instantly.
  setTimeout(reDisplay, 100);
});
