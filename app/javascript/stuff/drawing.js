// state :: [Int]
export function renderCubeState(state, app) {
  let canvas = document.getElementById("cube-display-canvas");
  // there's something weird going on here with the canvas aspect ratio. I don't understand it,
  // but the following 3 lines appear to fix the problem.
  let bounds = canvas.parentElement.getBoundingClientRect();
  canvas.height = bounds.height;
  canvas.width = bounds.width;
  // actual ratio: image width = r * image height. r = 2*(sqrt(2) - 1)
  let r = 2*(Math.SQRT2 - 1);
  // canvas height is limiting factor
  let figureHeight = canvas.height;
  let figureWidth = figureHeight * r;
  let sideLen = figureHeight / ((3/2)*Math.SQRT2 + 2);
  let startX = canvas.width / 2;
  let startY = sideLen * (1 - (Math.SQRT2 / 2));
  if (figureWidth > canvas.width) {
    // canvas width is limiting factor
    figureWidth = canvas.width;
    figureHeight = figureWidth / r;
    sideLen = figureWidth / (2 + Math.SQRT2);
    startX = canvas.width / 2;
    startY = canvas.height / 2 - (sideLen * (((3/4)*Math.SQRT2) + 1)) + (sideLen * (1 - Math.SQRT2 / 2));
  }
  
  let ctx = canvas.getContext("2d");
  ctx.fillStyle = "rgb(0, 0, 0)";
  let startPoint = [startX, startY];
  
  let d = sideLen / Math.SQRT2;
  let margin = 3; // don't need to turn into relative
  // RUF skeleton
  ctx.beginPath();
  ctx.moveTo(...startPoint);
  let pt1 = [startPoint[0] - d, startPoint[1] + d] 
  ctx.lineTo(...pt1);
  let pt2 = [pt1[0], pt1[1] + sideLen];
  ctx.lineTo(...pt2);
  let pt3 = [startPoint[0], pt2[1] + d];
  ctx.lineTo(...pt3);
  let pt4 = [pt3[0] + d, pt3[1] - d]
  ctx.lineTo(...pt4);
  let pt5 = [pt4[0], pt4[1] - sideLen];
  ctx.lineTo(...pt5);
  ctx.closePath();
  ctx.fill();

  let midpt = [pt3[0], pt3[1] - sideLen]
  // D skeleton
  ctx.beginPath();
  ctx.moveTo(...pt3);
  let pt6 = [pt3[0] - d, pt3[1] + d];
  ctx.lineTo(...pt6);
  let pt7 = [pt3[0], pt6[1] + d];
  ctx.lineTo(...pt7);
  let pt8 = [pt7[0] + d, pt6[1]];
  // console.log(pt3, pt6, pt7, pt8);
  ctx.lineTo(...pt8);
  ctx.closePath();
  ctx.fill();

  // B skeleton
  ctx.beginPath();
  ctx.moveTo(...pt5);
  let pt9 = [pt5[0], pt5[1] - sideLen];
  ctx.lineTo(...pt9);
  let pt10 = [pt9[0] + d, pt9[1] + d];
  ctx.lineTo(...pt10);
  let pt11 = [pt10[0], pt10[1] + sideLen];
  ctx.lineTo(...pt11);
  ctx.closePath();
  ctx.fill();

  // L skeleton
  ctx.beginPath();
  ctx.moveTo(...pt1);
  let pt12 = [pt1[0], pt1[1] - sideLen];
  ctx.lineTo(...pt12);
  let pt13 = [pt12[0] - d, pt12[1] + d];
  ctx.lineTo(...pt13);
  let pt14 = [pt13[0], pt13[1] + sideLen];
  ctx.lineTo(...pt14);
  // console.log(pt1, pt12, pt13, pt14);
  ctx.closePath();
  ctx.fill();

  let sl = (sideLen - 6 * margin) / 3
  let sd = sl / Math.SQRT2;
  function drawRLSticker(topLeft, color) {
    ctx.beginPath();
    let rs1 = topLeft;
    ctx.moveTo(...rs1);
    let rs2 = [rs1[0] + sd, rs1[1] - sd];
    ctx.lineTo(...rs2);
    let rs3 = [rs2[0], rs2[1] + sl];
    ctx.lineTo(...rs3);
    let rs4 = [rs1[0], rs1[1] + sl]
    ctx.lineTo(...rs4);
    ctx.closePath();
    ctx.fillStyle = app.colors[color];
    ctx.fill();
  }

  function drawUDSticker(top, color) {
    ctx.beginPath();
    ctx.moveTo(...top);
    let r = [top[0] + sd, top[1] + sd];
    ctx.lineTo(...r);
    let d = [r[0] - sd, r[1] + sd];
    ctx.lineTo(...d);
    let l = [d[0] - sd, d[1] - sd];
    ctx.lineTo(...l);
    ctx.closePath();
    ctx.fillStyle = app.colors[color];
    ctx.fill();
  }

  function drawFBSticker(topLeft, color) {
    ctx.beginPath();
    ctx.moveTo(...topLeft);
    let r = [topLeft[0] + sd, topLeft[1] + sd];
    ctx.lineTo(...r);
    let d = [r[0], r[1] + sl];
    ctx.lineTo(...d);
    let l = [topLeft[0], topLeft[1] + sl];
    ctx.lineTo(...l);
    ctx.closePath();
    ctx.fillStyle = app.colors[color];
    ctx.fill();
  }
 
  let diff = sl + 2 * margin;

  let r1 = [midpt[0] + margin, midpt[1]];
  drawRLSticker(r1, state[0]);
  let r2 = [r1[0], r1[1] + diff];
  drawRLSticker(r2, state[3]);
  let r3 = [r2[0], r2[1] + diff];
  drawRLSticker(r3, state[6]);

  let r4 = [r1[0] + sd + margin, r1[1] - sd - margin];
  drawRLSticker(r4, state[1]);
  let r5 = [r4[0], r4[1] + diff];
  drawRLSticker(r5, state[4]);
  let r6 = [r5[0], r5[1] + diff];
  drawRLSticker(r6, state[7]);

  let r7 = [r4[0] + sd + margin, r4[1] - sd - margin];
  drawRLSticker(r7, state[2]);
  let r8 = [r7[0], r7[1] + diff];
  drawRLSticker(r8, state[5]);
  let r9 = [r8[0], r8[1] + diff];
  drawRLSticker(r9, state[8]);

  let m1 = Math.SQRT2 * margin;
  let diff1 = diff / Math.SQRT2;

  let u1 = [startPoint[0], startPoint[1] + m1];
  drawUDSticker(u1, state[11]);
  let u2 = [u1[0] - diff1, u1[1] + diff1];
  drawUDSticker(u2, state[10]);
  let u3 = [u2[0] - diff1, u2[1] + diff1];
  drawUDSticker(u3, state[9]);

  let u4 = [u1[0] + diff1, u1[1] + diff1];
  drawUDSticker(u4, state[14]);
  let u5 = [u4[0] - diff1, u4[1] + diff1];
  drawUDSticker(u5, state[13]);
  let u6 = [u5[0] - diff1, u5[1] + diff1];
  drawUDSticker(u6, state[12]);

  let u7 = [u4[0] + diff1, u4[1] + diff1]; 
  drawUDSticker(u7, state[17]);
  let u8 = [u7[0] - diff1, u7[1] + diff1]
  drawUDSticker(u8, state[16]);
  let u9 = [u8[0] - diff1, u8[1] + diff1];
  drawUDSticker(u9, state[15]);

  let f1 = [pt2[0] + margin, pt2[1] - sl];
  drawFBSticker(f1, state[24]);
  let f2 = [f1[0], f1[1] - diff];
  drawFBSticker(f2, state[21]);
  let f3 = [f2[0], f2[1] - diff];
  drawFBSticker(f3, state[18]);

  let f4 = [f3[0] + sd + margin, f3[1] + sd + margin];
  drawFBSticker(f4, state[19]);
  let f5 = [f4[0], f4[1] + diff]
  drawFBSticker(f5, state[22]);
  let f6 = [f5[0], f5[1] + diff]
  drawFBSticker(f6, state[25]);

  let f7 = [f4[0] + sd + margin, f4[1] + sd + margin];
  drawFBSticker(f7, state[20]);
  let f8 = [f7[0], f7[1] + diff]
  drawFBSticker(f8, state[23]);
  let f9 = [f8[0], f8[1] + diff]
  drawFBSticker(f9, state[26]);
 
  let l1 = [pt13[0] + margin, pt13[1]];
  drawRLSticker(l1, state[27]);
  let l2 = [l1[0], l1[1] + diff];
  drawRLSticker(l2, state[30]);
  let l3 = [l2[0], l2[1] + diff];
  drawRLSticker(l3, state[33]);

  let l4 = [l1[0] + sd + margin, l1[1] - sd - margin];
  drawRLSticker(l4, state[28]);
  let l5 = [l4[0], l4[1] + diff];
  drawRLSticker(l5, state[31]);
  let l6 = [l5[0], l5[1] + diff];
  drawRLSticker(l6, state[34]);

  let l7 = [l4[0] + sd + margin, l4[1] - sd - margin];
  drawRLSticker(l7, state[29]);
  let l8 = [l7[0], l7[1] + diff];
  drawRLSticker(l8, state[32]);
  let l9 = [l8[0], l8[1] + diff];
  drawRLSticker(l9, state[35]);

  let d1 = [pt3[0], pt3[1] + m1];
  drawUDSticker(d1, state[38]);
  let d2 = [d1[0] - diff1, d1[1] + diff1];
  drawUDSticker(d2, state[37]);
  let d3 = [d2[0] - diff1, d2[1] + diff1];
  drawUDSticker(d3, state[36]);

  let d4 = [d1[0] + diff1, d1[1] + diff1];
  drawUDSticker(d4, state[41]);
  let d5 = [d4[0] - diff1, d4[1] + diff1];
  drawUDSticker(d5, state[40]);
  let d6 = [d5[0] - diff1, d5[1] + diff1];
  drawUDSticker(d6, state[39]);

  let d7 = [d4[0] + diff1, d4[1] + diff1];
  drawUDSticker(d7, state[44]);
  let d8 = [d7[0] - diff1, d7[1] + diff1];
  drawUDSticker(d8, state[43]);
  let d9 = [d8[0] - diff1, d8[1] + diff1];
  drawUDSticker(d9, state[42]);

  let b1 = [pt5[0] + margin, pt5[1] - sl];
  drawFBSticker(b1, state[51]);
  let b2 = [b1[0], b1[1] - diff];
  drawFBSticker(b2, state[48]);
  let b3 = [b2[0], b2[1] - diff];
  drawFBSticker(b3, state[45]);

  let b4 = [b3[0] + sd + margin, b3[1] + sd + margin];
  drawFBSticker(b4, state[46]);
  let b5 = [b4[0], b4[1] + diff];
  drawFBSticker(b5, state[49]);
  let b6 = [b5[0], b5[1] + diff];
  drawFBSticker(b6, state[52]);

  let b7 = [b4[0] + sd + margin, b4[1] + sd + margin];
  drawFBSticker(b7, state[47]);
  let b8 = [b7[0], b7[1] + diff];
  drawFBSticker(b8, state[50]);
  let b9 = [b8[0], b8[1] + diff];
  drawFBSticker(b9, state[53]);
}
