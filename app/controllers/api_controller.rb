require 'pp'
class ApiController < ApplicationController
  # for ease of testing only
  skip_before_action :verify_authenticity_token

  def step
    # for first step, expecting params[:path] = [],
    # :cube_state = solved_state
    # :solution = ''
    # :step = 0
    # 
    if params[:path] == []
      scramble = Scramble.new
      params[:cube_state] = CubeState.from_scramble(scramble).to_array
      render json: {
        scramble: scramble.to_s
    }.merge(Solver.new(params).solve)
    else
      render json: Solver.new(params).solve
    end
  end
end
