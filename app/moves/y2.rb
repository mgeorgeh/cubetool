class Y2 < Rotation
  def self.cycle_hash
    {
      0 => 3,
      3 => 0,
      2 => 5,
      5 => 2,
      1 => 1,
      4 => 4
    }
  end

  def self.inverse
    Y2
  end
end