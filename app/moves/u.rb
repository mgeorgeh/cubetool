class U < Move
  def self.cycle_hash
    {
      0 => 2,
      2 => 3,
      3 => 5,
      5 => 0,
      1 => 1
    }
  end

  def self.this_coord
    1
  end

  def self.inverse
    Ui
  end
end
