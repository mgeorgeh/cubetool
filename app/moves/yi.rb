class Yi < Rotation
  def self.cycle_hash
    {
      0 => 5,
      5 => 3,
      3 => 2,
      2 => 0,
      1 => 1,
      4 => 4
    }
  end

  def self.inverse
    Y
  end
end