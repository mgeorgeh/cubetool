class Reflection
  def self.transform(data)
    d = {}
    data.each do |coord, face|
      d[cycle_hash[coord]] = cycle_hash[face]
    end
    d
  end

  def self.face_lookup
    {
      'R' => 0,
      'U' => 1,
      'F' => 2,
      'L' => 3,
      'D' => 4,
      'B' => 5
    }
  end

  def self.change_move(move)
    cs = move.to_s.chars
    new_c = face_lookup.key(cycle_hash[face_lookup[cs[0]]])
    cs[0] = new_c
    cs.join.constantize.inverse
  end
end