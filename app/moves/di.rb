class Di < Move
  def self.cycle_hash
    {
      0 => 2,
      2 => 3,
      3 => 5,
      5 => 0,
      4 => 4
    }
  end

  def self.this_coord
    4
  end

  def self.inverse
    D
  end
end
