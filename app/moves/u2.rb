class U2 < Move
  def self.cycle_hash
    {
      0 => 3,
      3 => 0,
      2 => 5,
      5 => 2,
      1 => 1
    }
  end

  def self.this_coord
    1
  end

  def self.inverse
    U2
  end
end
