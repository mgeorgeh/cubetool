class F < Move
  def self.cycle_hash
    {
      0 => 4,
      4 => 3,
      3 => 1,
      1 => 0,
      2 => 2
    }
  end

  def self.this_coord
    2
  end

  def self.inverse
    Fi
  end
end
