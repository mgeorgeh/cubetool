class RLRef < Reflection
  def self.cycle_hash
    {
      0 => 3,
      1 => 1,
      2 => 2,
      3 => 0,
      4 => 4,
      5 => 5
    }
  end

  def self.inverse
    RLRef
  end
end