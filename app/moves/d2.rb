class D2 < Move
  def self.cycle_hash
    {
      0 => 3,
      3 => 0,
      2 => 5,
      5 => 2,
      4 => 4
    }
  end

  def self.this_coord
    4
  end

  def self.inverse
    D2
  end
end
