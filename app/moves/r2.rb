class R2 < Move
  def self.cycle_hash
    {
      1 => 4,
      4 => 1,
      2 => 5,
      5 => 2,
      0 => 0
    }
  end

  def self.this_coord
    0
  end

  def self.inverse
    R2
  end
end
