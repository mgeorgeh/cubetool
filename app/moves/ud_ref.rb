class UDRef < Reflection
  def self.cycle_hash
    {
      0 => 0,
      1 => 4,
      2 => 2,
      3 => 3,
      4 => 1,
      5 => 5
    }
  end

  def self.inverse
    UDRef
  end
end