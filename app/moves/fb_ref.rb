class FBRef < Reflection
  def self.cycle_hash
    {
      0 => 0,
      1 => 1,
      2 => 5,
      3 => 3,
      4 => 4,
      5 => 2,
    }
  end

  def self.inverse
    FBRef
  end
end