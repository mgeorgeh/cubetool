class F2 < Move
  def self.cycle_hash
    {
      0 => 3,
      3 => 0,
      1 => 4,
      4 => 1,
      2 => 2
    }
  end

  def self.this_coord
    2
  end

  def self.inverse
    F2
  end
end
