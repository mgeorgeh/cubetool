class Ui < Move
  def self.cycle_hash
    {
      0 => 5,
      5 => 3,
      3 => 2,
      2 => 0,
      1 => 1
    }
  end

  def self.this_coord
    1
  end

  def self.inverse
    U
  end
end
