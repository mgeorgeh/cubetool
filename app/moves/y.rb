class Y < Rotation
  def self.cycle_hash
    {
      0 => 2,
      2 => 3,
      3 => 5,
      5 => 0,
      1 => 1,
      4 => 4
    }
  end

  def self.inverse
    Yi
  end
end