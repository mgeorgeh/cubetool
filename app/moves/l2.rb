class L2 < Move
  def self.cycle_hash
    {
      1 => 4,
      4 => 1,
      2 => 5,
      5 => 2,
      3 => 3
    }
  end

  def self.this_coord
    3
  end

  def self.inverse
    L2
  end
end
