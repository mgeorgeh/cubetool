class B < Move
  def self.cycle_hash
    {
      0 => 1,
      1 => 3,
      3 => 4,
      4 => 0,
      5 => 5
    }
  end

  def self.this_coord
    5
  end

  def self.inverse
    Bi
  end
end
