class Bi < Move
  def self.cycle_hash
    {
      0 => 4,
      4 => 3,
      3 => 1,
      1 => 0,
      5 => 5
    }
  end

  def self.this_coord
    5
  end

  def self.inverse
    B
  end
end
