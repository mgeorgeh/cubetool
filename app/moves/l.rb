class L < Move
  def self.cycle_hash
    {
      1 => 2,
      2 => 4,
      4 => 5,
      5 => 1,
      3 => 3
    }
  end

  def self.this_coord
    3
  end

  def self.inverse
    Li
  end
end
