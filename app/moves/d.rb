class D < Move
  def self.cycle_hash
    {
      0 => 5,
      5 => 3,
      3 => 2,
      2 => 0,
      4 => 4
    }
  end

  def self.this_coord
    4
  end

  def self.inverse
    Di
  end
end
