class Z2 < Rotation
  def self.cycle_hash
    {
      0 => 3,
      3 => 0,
      1 => 4,
      4 => 1,
      2 => 2,
      5 => 5
    }
  end

  def self.inverse
    Z2
  end
end