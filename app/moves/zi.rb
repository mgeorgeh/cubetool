class Zi < Rotation
  def self.cycle_hash
    {
      0 => 1,
      1 => 3,
      3 => 4,
      4 => 0,
      2 => 2,
      5 => 5
    }
  end

  def self.inverse
    Z
  end
end