class Li < Move
  def self.cycle_hash
    {
      1 => 5,
      5 => 4,
      4 => 2,
      2 => 1,
      3 => 3
    }
  end

  def self.this_coord
    3
  end

  def self.inverse
    L
  end
end
