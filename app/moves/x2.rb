class X2 < Rotation
  def self.cycle_hash
    {
      1 => 4,
      4 => 1,
      2 => 5,
      5 => 2,
      0 => 0,
      3 => 3
    }
  end

  def self.inverse
    X2
  end
end