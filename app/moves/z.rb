class Z < Rotation
  def self.cycle_hash
    {
      0 => 4,
      4 => 3,
      3 => 1,
      1 => 0,
      2 => 2,
      5 => 5,
    }
  end

  def self.inverse
    Zi
  end
end