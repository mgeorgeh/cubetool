class Xi < Rotation
  def self.cycle_hash
    {
      1 => 2,
      2 => 4,
      4 => 5,
      5 => 1,
      0 => 0,
      3 => 3
    }
  end

  def self.inverse
    X
  end
end