class R < Move
  def self.cycle_hash
    {
      1 => 5,
      5 => 4,
      4 => 2,
      2 => 1,
      0 => 0
    }
  end

  def self.this_coord
    0
  end

  def self.inverse
    Ri
  end
end
