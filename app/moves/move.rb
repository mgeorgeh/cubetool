class Move
  def self.transform(data)
    return data unless data.include?(this_coord)
    d = {}
    data.each do |coord, face|
      d[cycle_hash[coord]] = face
    end
    d
  end

  def self.from_string(str)
    if ["x", "y", "z"].include?(str.chars[0])
      Rotation.from_string(str)
    else
      str.tr("'", 'i').constantize
    end
  end

  def self.all
    [
      R, Ri, R2,
      U, Ui, U2,
      F, Fi, F2,
      L, Li, L2,
      D, Di, D2,
      B, Bi, B2,
    ]
  end
end
