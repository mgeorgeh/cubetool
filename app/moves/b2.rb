class B2 < Move
  def self.cycle_hash
    {
      1 => 4,
      4 => 1,
      0 => 3,
      3 => 0,
      5 => 5
    }
  end

  def self.this_coord
    5
  end

  def self.inverse
    B2
  end
end
