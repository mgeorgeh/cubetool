class X < Rotation
  def self.cycle_hash
    {
      1 => 5,
      5 => 4,
      4 => 2,
      2 => 1,
      0 => 0,
      3 => 3
    }
  end

  def self.inverse
    Xi
  end
end