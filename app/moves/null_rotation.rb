class NullRotation < Rotation
  def self.cycle_hash
    {
      0 => 0,
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
    }
  end

  def self.inverse
    self
  end
end