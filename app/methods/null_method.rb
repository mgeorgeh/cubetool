class NullMethod < CubeMethod
  def valid?
    false
  end

  def valid_method_step(_step)
    false
  end

  def step_solved?(_step, _cube_state)
    false
  end

  def step_solvable_using_moveset(_step, _move_set)
    false
  end
end
