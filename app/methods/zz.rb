class ZZ < CubeMethod
  attr_accessor :graph
  def initialize
    @graph = {
      EoLineState => [ZzS2State],
      ZzS2State   => [ZzS3aState, ZzS3bState, ZzS3cState],
      ZzS3aState  => [ZzS4aState],
      ZzS3bState  => [ZzS4bState],
      ZzS3cState  => [ZzS4cState],
      ZzS4aState  => [ZzS5State],
      ZzS4bState  => [ZzS5State],
      ZzS4cState  => [ZzS5State],
      ZzS5State   => [ZbllState],
    }
    @start_step = EoLineState
  end
end
