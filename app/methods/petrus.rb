class Petrus < CubeMethod
  def initialize
    @graph = {
      PetrusS1State => [PetrusS2State],
      PetrusS2State => [PetrusS3State],
      PetrusS3State => [PetrusS4State],
      PetrusS4State => [PetrusS5State],
      PetrusS5State => [ZbllState],
    }
    @start_step = PetrusS1State
  end
end
