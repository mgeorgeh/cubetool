class Roux < CubeMethod
  def initialize
    @graph = {
      RouxS1State => [RouxS2State],
      RouxS2State => [RouxS3State],
      RouxS3State => [RouxS4State],
      RouxS4State => [RouxS5State],
      RouxS5State => [RouxS6State],
      RouxS6State => [RouxS7State],
    }
    @start_step = RouxS1State
  end
end
