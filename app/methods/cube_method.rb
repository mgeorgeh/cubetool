class CubeMethod
  def self.from_string(str)
    return NullMethod if !str.is_a? String
    lookup_hash.fetch(str, NullMethod).new
  end

  def self.valid_names
    lookup_hash.values.map(&:to_s)
  end

  def solutions_for(path:, state:)
    trs = path.transforms
    transformed_state = CubeState.by_applying(trs, to: state)
    pp next_steps(path)
    next_steps(path).flat_map do |step|
      step.varying_transforms.map do |t|
        full_transform = trs.concatenating(t)
        # label generation is probably in the wrong spot
        label = step.label.map do |p| 
          Piece.solved(p).applying_moves(full_transform.inverse).lookup_string 
        end.join("/") 

        sols = MethodStepExtractor.new(
          state_type: step, 
          cube_state: CubeState.by_applying(t, to: transformed_state)
        ).extract.solutions
         .map { |sol| full_transform.inverse.apply_to(sol) }

        {
          label: label,
          transform: t.to_json_output,
          step_type: step.to_s,
          solutions: sols.map { |sol| sol.to_s },
          solution_length: sols[0].count,
        }
      end
    end
  end

  def next_steps(path)
    path.empty? ? [@start_step] : (@graph[path.last[:step_type]] || [])
  end

  def step_solved?(path, cube_state)
    true
  end

  def valid?
    true
  end

  def valid_method_step(step)
    # implement
  end

  def step_solved?(step, cube_state)
    # implement
  end

  def self.lookup_hash
    {
      'Fridrich' => Fridrich,
      'Petrus' => Petrus,
      'ZZ' => ZZ,
      'Roux' => Roux
    }
  end
end
