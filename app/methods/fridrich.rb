class Fridrich < CubeMethod
  def initialize
    @graph = {
      CrossState => [Cep1State],
      Cep1State => [Cep2aState, Cep2bState],
      Cep2aState => [Cep3aState],
      Cep2bState => [Cep3bState],
      Cep3aState => [Cep4State],
      Cep3bState => [Cep4State],
      Cep4State => [OllState],
      OllState => [PllState]
    }
    @start_step = CrossState
  end
end
